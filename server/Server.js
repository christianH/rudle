/* eslint-disable no-console */
import { ApolloServer } from 'apollo-server';
import typeDefs from './src/graphql/typeDefs';
import resolvers from './src/graphql/resolvers';
import { decodeToken } from './src/services/auth';
import './src/db';

const Server = new ApolloServer({
  typeDefs,
  resolvers,
  introspection: true,
  playground: true,
  subscriptions: {
    onConnect: connectionParams => {
      if (connectionParams.token) {
        const { token } = connectionParams;

        const user = decodeToken(token);
        return { user: user._id };
      }
      throw new Error('Missing auth token!');
    }
  },
  context: async ({ req, connection }) => {
    if (connection) {
      return connection.context;
    }
    const token = req.headers.authorization || '';
    const user = decodeToken(token);

    return { user };
  }
});

Server.listen().then(({ url, subscriptionsUrl }) => {
  console.log(`🚀 Server ready at ${url}`);
  console.log(`🚀 Subscriptions ready at ${subscriptionsUrl}`);
});
