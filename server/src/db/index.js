import mongoose from 'mongoose';

import { DB_URL } from '../config';

mongoose.Promise = global.Promise;
mongoose.set('useFindAndModify', false);
mongoose.set('debug', true); // debug mode on

try {
  mongoose.connect(
    DB_URL,
    { useNewUrlParser: true }
  );
} catch (err) {
  mongoose.createConnection(DB_URL, { useNewUrlParser: true });
}

mongoose.connection
  // eslint-disable-next-line no-console
  .once('open', () => console.log('MongoDB running'))
  .on('error', e => {
    throw e;
  });
