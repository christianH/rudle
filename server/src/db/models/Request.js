import mongoose, { Schema } from 'mongoose';

const RequestSchema = new Schema(
  {
    activity: {
      type: Schema.Types.ObjectId,
      ref: 'Activity'
    },
    user: {
      type: String,
      ref: 'User'
    },
    seenBy: { type: Array, default: [] },
    accessDenied: { type: Array, default: [] },
    accessAllowed: { type: Array, default: [] }
  },
  { timestamps: true }
);

export default mongoose.model('Request', RequestSchema);
