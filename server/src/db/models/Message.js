import mongoose, { Schema } from 'mongoose';

const MessageSchema = new Schema(
  {
    chatId: String,
    activityId: String,
    from: String,
    text: String
  },
  { timestamps: true }
);

export default mongoose.model('Message', MessageSchema);
