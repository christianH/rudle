import mongoose, { Schema } from 'mongoose';

const ChatSchema = new Schema(
  {
    members: { type: Array, default: [] },
    seenBy: { type: Array, default: [] },
    lastMessageAt: { type: Date, default: Date.now }
  },
  { timestamps: true }
);

export default mongoose.model('Chat', ChatSchema);
