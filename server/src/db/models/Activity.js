import mongoose, { Schema } from 'mongoose';

const ActivitySchema = new Schema(
  {
    title: String,
    subtitle: String,
    description: String,
    image: String,
    totalRequests: {
      type: Number,
      default: 0
    }
  },
  { timestamps: true }
);

export default mongoose.model('Activity', ActivitySchema);
