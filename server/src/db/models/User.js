import mongoose, { Schema } from 'mongoose';
import jwt from 'jsonwebtoken';

import { JWT_SECRET } from '../../config';

const UserSchema = new Schema(
  {
    _id: String,
    name: String,
    gender: String,
    birthday: Date,
    about: String,
    avatar: String,
    pictures: { type: Array, default: [] },
    location: {
      latitude: { type: Number, default: 0.0 },
      longitude: { type: Number, default: 0.0 }
    },
    settings: {
      radius: { type: Number, default: 40 },
      connectWithFavorites: { type: Boolean, default: true },
      connectWithFriends: { type: Boolean, default: true },
      connectWithFriendsOfFriends: { type: Boolean, default: false },
      connectWithUnknown: { type: Boolean, default: false },
      notificationRequests: { type: Boolean, default: true },
      notificationMessages: { type: Boolean, default: true }
    },
    friends: { type: Array, default: [] },
    favorites: { type: Array, default: [] },
    interests: { type: Array, default: [] },
    fbToken: String,
    pushToken: String
  },
  { timestamps: true }
);

UserSchema.methods = {
  createToken() {
    return jwt.sign(
      {
        _id: this._id
      },
      JWT_SECRET
    );
  }
};

export default mongoose.model('User', UserSchema);
