import { gql } from 'apollo-server';

const typeDefs = gql`
  scalar Date

  type Status {
    message: String!
  }

  type Location {
    latitude: Float!
    longitude: Float!
  }

  type Settings {
    radius: Int
    connectWithFavorites: Boolean
    connectWithFriends: Boolean
    connectWithFriendsOfFriends: Boolean
    connectWithUnknown: Boolean
    notificationRequests: Boolean
    notificationMessages: Boolean
  }

  type Interest {
    activity: Activity!
    count: Float!
  }

  type User {
    _id: String!
    name: String!
    gender: String
    birthday: Date
    avatar: String
    about: String
    pictures: [String]
    location: Location
    friends: [String]
    interests: [Interest]
    createdAt: Date!
    updatedAt: Date!
  }

  type Me {
    _id: String!
    name: String!
    gender: String
    birthday: Date
    avatar: String
    about: String
    pictures: [String]
    location: Location
    settings: Settings
    friends: [String]
    favorites: [String]
    interests: [Interest]
    fbToken: String
    pushToken: String
    createdAt: Date!
    updatedAt: Date!
  }

  type Auth {
    token: String!
  }

  type Activity {
    _id: ID!
    title: String!
    subtitle: String!
    description: String!
    image: String!
    totalRequests: Int!
    createdAt: Date!
    updatedAt: Date!
  }

  type Request {
    _id: ID!
    activity: Activity!
    user: User!
    accessDenied: [String]
    accessAllowed: [String]
    seenBy: [String]
    createdAt: Date!
    updatedAt: Date!
  }

  type Message {
    _id: ID!
    from: String!
    chatId: String!
    text: String!
    activityId: String
    createdAt: Date!
    updatedAt: Date!
  }

  type Chat {
    _id: ID!
    members: [String]
    messages: [Message]
    seenBy: [String]
    lastMessageAt: Date!
    createdAt: Date!
    updatedAt: Date!
  }

  type Query {
    me: Me
    getUser(_id: String!): User
    getActivities: [Activity]
    getActivity(_id: ID!): Activity
    getRequests: [Request]
    getRequest(_id: ID!): Request
    getMyLastRequest: Request
    getChats: [Chat]
    getChat(_id: ID!): Chat
  }

  type Mutation {
    login(fbToken: String!): Auth
    reconnect: Auth
    logout: Status
    updateUserLocation(latitude: Float!, longitude: Float!): Status
    updateUserPushToken(pushToken: String!): Status
    addUserToFavorites(userId: String!): Status
    removeUserFromFavorites(userId: String!): Status
    sendRequest(activityId: String!): Request
    sendMessage(userId: String, text: String!, activityId: String, createdAt: String!): Message
    markChatAsSeen(chatId: String!): Status
    markRequestAsSeen(requestId: String!): Status
    updateUserSettings(
      radius: Int
      connectWithFavorites: Boolean
      connectWithFriends: Boolean
      connectWithFriendsOfFriends: Boolean
      connectWithUnknown: Boolean
      notificationRequests: Boolean
      notificationMessages: Boolean
    ): Status
  }

  type Subscription {
    requestAdded: Request
    messageAdded: Chat
  }
`;

export default typeDefs;
