import User from '../../db/models/User';
import Activity from '../../db/models/Activity';
import { requireAuth } from '../../services/auth';
import { getFacebookUserData } from '../../services/facebook';

export default {
  login: async (_, { fbToken }) => {
    try {
      const userData = await getFacebookUserData(fbToken);
      const userObj = await User.findByIdAndUpdate(userData._id, userData, {
        new: true,
        upsert: true
      });
      const auth = { token: userObj.createToken() };
      return auth;
    } catch (error) {
      throw error;
    }
  },

  reconnect: async (_, args, { user }) => {
    try {
      await requireAuth(user);
      const { fbToken } = await User.findById(user._id);
      const userData = await getFacebookUserData(fbToken);
      const userObj = await User.findByIdAndUpdate(userData._id, userData, {
        new: true,
        upsert: true
      });
      const auth = { token: userObj.createToken() };
      return auth;
    } catch (error) {
      throw error;
    }
  },
  logout: async (_, args, { user }) => {
    try {
      await requireAuth(user);
      await User.findOneAndUpdate(
        { _id: user._id },
        { $set: { pushToken: '' } },
        { new: true, upsert: true }
      );

      return { message: 'Logout successful!' };
    } catch (error) {
      throw error;
    }
  },

  me: async (_, args, { user }) => {
    try {
      const me = await requireAuth(user);
      return me;
    } catch (error) {
      throw error;
    }
  },

  getUser: async (_, { _id }, { user }) => {
    try {
      await requireAuth(user);
      return User.findById(_id);
    } catch (error) {
      throw error;
    }
  },

  updateUserPushToken: async (_, { pushToken }, { user }) => {
    try {
      await requireAuth(user);
      await User.findOneAndUpdate(
        { _id: user._id },
        { $set: { pushToken } },
        { new: true, upsert: true }
      );

      return { message: 'PushToken updated!' };
    } catch (error) {
      throw error;
    }
  },

  updateUserLocation: async (_, args, { user }) => {
    try {
      await requireAuth(user);
      await User.findOneAndUpdate(
        { _id: user._id },
        { $set: { 'location.latitude': args.latitude, 'location.longitude': args.longitude } },
        { new: true, upsert: true }
      );

      return { message: 'Location updated!' };
    } catch (error) {
      throw error;
    }
  },

  updateUserSettings: async (_, args, { user }) => {
    try {
      await requireAuth(user);

      const userObj = await User.findById(user._id);
      const newSettings = Object.assign({}, userObj.settings, args);
      await User.findOneAndUpdate(
        { _id: user._id },
        { $set: { settings: newSettings } },
        { upsert: true }
      );

      return { message: 'Settings updated!' };
    } catch (error) {
      throw error;
    }
  },

  addUserToFavorites: async (_, { userId }, { user }) => {
    try {
      await requireAuth(user);
      await User.findOneAndUpdate({ _id: user._id }, { $addToSet: { favorites: userId } });
      return { message: 'Added to Favorites!' };
    } catch (error) {
      throw error;
    }
  },

  removeUserFromFavorites: async (_, { userId }, { user }) => {
    try {
      await requireAuth(user);
      await User.findOneAndUpdate({ _id: user._id }, { $pull: { favorites: userId } });
      return { message: 'Removed From Favorites!' };
    } catch (error) {
      throw error;
    }
  },

  activity: async ({ activity }) => Activity.findById(activity)
};
