import Activity from '../../db/models/Activity';
import { requireAuth } from '../../services/auth';

export default {
  getActivities: async (_, args, { user }) => {
    try {
      await requireAuth(user);
      return Activity.find({}).sort({ totalRequests: -1 });
    } catch (error) {
      throw error;
    }
  },

  getActivity: async (_, { _id }, { user }) => {
    try {
      await requireAuth(user);
      return Activity.findById(_id);
    } catch (error) {
      throw error;
    }
  }
};
