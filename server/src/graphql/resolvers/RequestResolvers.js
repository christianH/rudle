import { withFilter } from 'apollo-server';
import pubsub from '../../services/pubsub';
import Request from '../../db/models/Request';
import Activity from '../../db/models/Activity';
import User from '../../db/models/User';
import { requireAuth } from '../../services/auth';

const REQUEST_ADDED = 'REQUEST_ADDED';
const expire = 5000; // hours

export default {
  getRequests: async (_, args, { user }) => {
    try {
      await requireAuth(user);
      const timeInterval = new Date().setHours(new Date().getHours() - expire);
      // const timeInterval = new Date(new Date().setHours(0, 0, 0));
      return Request.find({
        user: { $ne: user._id },
        createdAt: { $gte: timeInterval },
        accessDenied: { $ne: user._id },
        $or: [{ accessAllowed: '*' }, { accessAllowed: user._id }]
      })
        .sort({ createdAt: -1 })
        .limit(20);
    } catch (error) {
      throw error;
    }
  },

  getRequest: async (_, { _id }, { user }) => {
    try {
      await requireAuth(user);
      return Request.findById(_id);
    } catch (error) {
      throw error;
    }
  },

  getMyLastRequest: async (_, args, { user }) => {
    try {
      await requireAuth(user);
      const timeInterval = new Date().setHours(new Date().getHours() - expire);
      // const timeInterval = new Date(new Date().setHours(0, 0, 0));
      return Request.findOne({ user: user._id, createdAt: { $gte: timeInterval } });
    } catch (error) {
      throw error;
    }
  },

  user: async ({ user }) => User.findById(user),
  activity: async ({ activity }) => Activity.findById(activity),

  sendRequest: async (_, { activityId }, { user }) => {
    try {
      await requireAuth(user);
      // UPDATE ACTIVITY COUNT
      const activity = await Activity.findByIdAndUpdate(
        { _id: activityId },
        { $inc: { totalRequests: 1 } },
        { new: true }
      );

      // UPDATE USER INTEREST COUNT
      const userObj = await User.findOne({ _id: user._id, 'interests.activity': activity._id });
      if (!userObj) {
        await User.findOneAndUpdate(
          { _id: user._id },
          {
            $addToSet: {
              interests: { activity: activity._id, count: 0 }
            }
          }
        );
      }
      await User.findOneAndUpdate(
        { _id: user._id, 'interests.activity': activity._id },
        { $inc: { 'interests.$.count': 1 } }
      );

      // SET PERMISSIONS
      let accessAllowed = [];
      let accessDenied = [];

      const getFriendsOfFriends = async u => {
        const arr = u.friends.map(async friendId => {
          const friendObj = await User.findById(friendId);
          const fofNotFriendsNotFavNotMe = await friendObj.friends.filter(
            x => !u.friends.includes(x) && !u.favorites.includes(x) && x !== u._id
          );
          return fofNotFriendsNotFavNotMe;
        });
        const fof = await Promise.all(arr);
        return fof.flat();
      };

      // Unknown
      if (userObj && userObj.settings.connectWithUnknown === true) {
        accessAllowed.push('*');
      }
      // Favorites
      if (userObj && userObj.settings.connectWithFavorites === true) {
        accessAllowed = [...accessAllowed, ...userObj.favorites];
      }
      if (userObj && userObj.settings.connectWithFavorites === false) {
        accessDenied = [...accessDenied, ...userObj.favorites];
      }
      // add Friends
      if (userObj && userObj.settings.connectWithFriends === true) {
        const friendsNotFav = userObj.friends.filter(x => !userObj.favorites.includes(x));
        accessAllowed = [...accessAllowed, ...friendsNotFav];
      }
      if (userObj && userObj.settings.connectWithFriends === false) {
        const friendsNotFav = userObj.friends.filter(x => !userObj.favorites.includes(x));
        accessDenied = [...accessDenied, ...friendsNotFav];
      }
      // add FriendsOfFriends
      if (userObj && userObj.settings.connectWithFriendsOfFriends === true) {
        const fof = await getFriendsOfFriends(userObj);
        accessAllowed = [...accessAllowed, ...fof];
      }
      if (userObj && userObj.settings.connectWithFriendsOfFriends === false) {
        const fof = await getFriendsOfFriends(userObj);
        accessDenied = [...accessDenied, ...fof];
      }
      // remove duplicates
      accessAllowed = [...new Set(accessAllowed)];
      accessDenied = [...new Set(accessDenied)];

      // TODO: add location to Request / accessDenied Users / blocked User
      const requestAdded = await Request.create({
        activity: activity._id,
        user: user._id,
        accessAllowed,
        accessDenied
      });

      // TODO: send PushNotification
      await pubsub.publish(REQUEST_ADDED, { requestAdded });
      return requestAdded;
    } catch (error) {
      throw error;
    }
  },

  markRequestAsSeen: async (_, { requestId }, { user }) => {
    try {
      await requireAuth(user);
      await Request.findByIdAndUpdate(requestId, { $addToSet: { seenBy: user._id } });
      return { message: 'Request Marked as Seen!' };
    } catch (error) {
      throw error;
    }
  },

  requestAdded: {
    subscribe: withFilter(
      () => pubsub.asyncIterator(REQUEST_ADDED),
      (payload, args, { user }) => {
        const isOwner = user === payload.requestAdded.user;

        const hasPermission =
          !payload.requestAdded.accessDenied.includes(user) &&
          (payload.requestAdded.accessAllowed.includes('*') ||
            payload.requestAdded.accessAllowed.includes(user));

        return Boolean(!isOwner && hasPermission); // TODO: wantsRequest?
      }
    )
  }
};
