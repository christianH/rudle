import GraphQLDate from 'graphql-date';
import UserResolvers from './UserResolvers';
import ActivityResolvers from './ActivityResolvers';
import RequestResolvers from './RequestResolvers';
import ChatResolvers from './ChatResolvers';

export default {
  Date: GraphQLDate,
  Request: {
    user: RequestResolvers.user,
    activity: RequestResolvers.activity
  },
  Interest: {
    activity: UserResolvers.activity
  },
  Chat: {
    messages: ChatResolvers.messages
  },
  Query: {
    me: UserResolvers.me,
    getUser: UserResolvers.getUser,
    getActivities: ActivityResolvers.getActivities,
    getActivity: ActivityResolvers.getActivity,
    getRequests: RequestResolvers.getRequests,
    getRequest: RequestResolvers.getRequest,
    getMyLastRequest: RequestResolvers.getMyLastRequest,
    getChats: ChatResolvers.getChats,
    getChat: ChatResolvers.getChat
  },
  Mutation: {
    login: UserResolvers.login,
    reconnect: UserResolvers.reconnect,
    logout: UserResolvers.logout,
    updateUserLocation: UserResolvers.updateUserLocation,
    updateUserSettings: UserResolvers.updateUserSettings,
    updateUserPushToken: UserResolvers.updateUserPushToken,
    addUserToFavorites: UserResolvers.addUserToFavorites,
    removeUserFromFavorites: UserResolvers.removeUserFromFavorites,
    sendRequest: RequestResolvers.sendRequest,
    sendMessage: ChatResolvers.sendMessage,
    markChatAsSeen: ChatResolvers.markChatAsSeen,
    markRequestAsSeen: RequestResolvers.markRequestAsSeen
  },
  Subscription: {
    requestAdded: RequestResolvers.requestAdded,
    messageAdded: ChatResolvers.messageAdded
  }
};
