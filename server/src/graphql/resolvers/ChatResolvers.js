import { withFilter } from 'apollo-server';
import pubsub from '../../services/pubsub';
import Chat from '../../db/models/Chat';
import User from '../../db/models/User';
import { requireAuth } from '../../services/auth';
import Message from '../../db/models/Message';
import { sendPushNotification } from '../../services/pushNotification';

const MESSAGE_ADDED = 'MESSAGE_ADDED';

export default {
  getChats: async (_, args, { user }) => {
    try {
      await requireAuth(user);
      const chats = await Chat.find({
        members: user._id
      }).sort({ lastMessageAt: -1 });
      return chats;
    } catch (error) {
      throw error;
    }
  },

  getChat: async (_, { _id }, { user }) => {
    try {
      await requireAuth(user);
      return Chat.findById(_id);
    } catch (error) {
      throw error;
    }
  },

  sendMessage: async (_, { userId, text, activityId, createdAt }, { user }) => {
    try {
      const me = await requireAuth(user);
      const updateOrCreateChatObj = async () => {
        const chatObj = await Chat.findOneAndUpdate(
          { members: { $all: [userId, user._id] } },
          {
            $set: {
              members: [userId, user._id],
              seenBy: [user._id],
              lastMessageAt: createdAt,
              activityId
            }
          },
          { new: true }
        );
        if (chatObj) return chatObj;

        const newChatObj = await Chat.create({
          members: [userId, user._id],
          seenBy: [user._id],
          lastMessageAt: createdAt,
          activityId
        });
        return newChatObj;
      };

      const chatObj = await updateOrCreateChatObj();
      const newMessage = await Message.create({
        from: user._id,
        chatId: chatObj._id,
        activityId,
        text,
        createdAt
      });

      // send PushNotification
      const memberObj = await User.findById(userId);
      if (memberObj.pushToken) {
        const pushNotificationData = {
          to: memberObj.pushToken,
          title: me.name,
          body: text,
          chatId: chatObj._id,
          from: user._id
        };
        sendPushNotification(pushNotificationData);
      }

      // publish message to subscription
      await pubsub.publish(MESSAGE_ADDED, { messageAdded: chatObj });
      return newMessage;
    } catch (error) {
      throw error;
    }
  },

  markChatAsSeen: async (_, { chatId }, { user }) => {
    try {
      await requireAuth(user);

      await Chat.findByIdAndUpdate(chatId, { $addToSet: { seenBy: user._id } });
      return { message: 'Chat Marked as Seen!' };
    } catch (error) {
      throw error;
    }
  },

  messages: async ({ _id }) => Message.find({ chatId: _id }).sort({ createdAt: -1 }),

  messageAdded: {
    subscribe: withFilter(
      () => pubsub.asyncIterator(MESSAGE_ADDED),
      (payload, args, { user }) => {
        const hasPermission = payload.messageAdded.members.includes(user);
        return Boolean(hasPermission);
      }
    )
  }
};
