import fetch from 'node-fetch';

export function sendPushNotification(data) {
  return fetch('https://exp.host/--/api/v2/push/send', {
    body: JSON.stringify({
      to: data.to,
      title: data.title,
      body: data.body,
      data: {
        from: data.from,
        title: data.title,
        body: data.body,
        chatId: data.chatId,
        requestId: data.requestId
      }
    }),
    headers: {
      host: 'exp.host',
      accept: 'application/json',
      'accept-encoding': 'gzip, deflate',
      'content-type': 'application/json'
    },
    method: 'POST'
  });
}

export const Test = 0;
