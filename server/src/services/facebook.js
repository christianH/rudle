import fetch from 'node-fetch';

export async function getFacebookUserData(fbToken) {
  const userData = await fetch(
    `https://graph.facebook.com/me?access_token=${fbToken}&fields=[
      'id',
      'name',
      'picture.width(9999)',
      'photos.limit(10){id,source}',
      'gender',
      'birthday',
      'friends',
    ]`
  );
  const avatar = await fetch(
    `https://graph.facebook.com/me?access_token=${fbToken}&fields=[
      'picture.width(150)',
    ]`
  );

  if (userData.ok && avatar.ok) {
    const userJSON = await userData.json();
    const avatarJSON = await avatar.json();
    const photoUrls = [userJSON.picture.data.url];

    if (userJSON.photos != null) {
      userJSON.photos.data.map(photo => photoUrls.push(photo.source));
    }

    const userObj = {
      _id: userJSON.id,
      name: userJSON.name,
      gender: userJSON.gender,
      birthday: new Date(userJSON.birthday),
      avatar: avatarJSON.picture.data.url,
      pictures: photoUrls,
      friends: userJSON.friends.data.map(friend => friend.id),
      fbToken
    };

    return userObj;
  }
  throw new Error('Invalid Facebook Token!');
}

export const Test = 0;
