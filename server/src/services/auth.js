import jwt from 'jsonwebtoken';

import { JWT_SECRET } from '../config';
import User from '../db/models/User';

export async function requireAuth(user) {
  if (!user || !user._id) throw new Error('Unauthorized');
  const me = await User.findById(user._id);
  if (!me) throw new Error('Unauthorized!');
  return me;
}

export function decodeToken(token) {
  try {
    let user = null;
    const arr = token.split(' ');
    if (arr[0] === 'Bearer') {
      user = jwt.verify(arr[1], JWT_SECRET);
    }
    return user;
  } catch (error) {
    throw new Error('Invalid Token!');
  }
}
