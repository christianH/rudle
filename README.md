# Rudle

With Rudle you can find Activities and let your Friends know what you want to do.

![rudle_preview](rudle_preview.png)

### Features

This project is currently under development.
It's built with React, React Native, Apollo Client, Expo, NodeJs, GraphQL, Apollo Server 2, MongoDB and offers:

- Facebook Auth
- Tinder-like Swiping
- Instagram-like Stories
- Friendship / Follower System
- Direct Messaging
- Push Notifications
- Realtime & Offline Functionality
- Geo Location & Tracking

### Getting started

Clone repository

```
$ git clone git@gitlab.com:christianH/rudle.git rudle-demo
```

### Installation

Install server node modules:

```
$ cd server
$ npm i
```

Install client node modules:

```
$ cd client
$ npm i
```

### Run Application

Start server:

```
$ cd server
$ npm run dev
```

Open and start project using Expo XDE or use the CLI:

```
$ cd client
$ expo start -c
```

Run on Android/iOS simulator or device:

```
$ expo ios
$ expo android
```

If you want to run the app on your physical device please change `SERVER_URL`; in `client/src/config/index.js`;

```
export const SERVER_URL = 'localhost:4000';
// export const SERVER_URL = '192.168.178.31:4000'; -> Your IP
```

### Login with Testuser:

```
user: jana_utsgmqd_hansen@tfbnw.net
pass: testuser_jana
```

```
user: ivana_vyrhvxd_russo@tfbnw.net
pass: testuser_ivana
```

Have fun 🍻
