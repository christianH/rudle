/* eslint-disable global-require */
import React, { Component } from 'react';
import { Text, TextInput, AsyncStorage, NetInfo } from 'react-native';
import { Asset, Font, AppLoading, Localization, ScreenOrientation } from 'expo';
import dayjs from 'dayjs';
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore';
import 'dayjs/locale/de';

import { ApolloProvider } from 'react-apollo';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { LoggedIn, LoggedOut } from './src/navigation';
import { reconnect, handleNetworkConnectionChange } from './src/lib';
import Client from './src/apollo';

const updateUserData = true; // DEV: no reconnect just login

export default class App extends Component {
  constructor(props) {
    super(props);
    // Disable FontScaling
    Text.defaultProps = {
      ...(Text.defaultProps || {}),
      allowFontScaling: false
    };
    TextInput.defaultProps = {
      ...(TextInput.defaultProps || {}),
      allowFontScaling: false
    };
  }

  state = {
    loading: true,
    loggedIn: false
  };

  componentDidMount() {
    ScreenOrientation.allowAsync(ScreenOrientation.Orientation.PORTRAIT_UP);
    NetInfo.addEventListener('connectionChange', handleNetworkConnectionChange);
  }

  componentWillUnmount() {
    NetInfo.removeEventListener('connectionChange');
  }

  startAsync = async () => {
    // Load Assets
    await Asset.loadAsync([
      require('./assets/images/background.png'),
      require('./assets/images/footer.png')
    ]);

    await Font.loadAsync({
      SignPainter: require('./assets/fonts/SignPainter.ttf'),
      Roboto_medium: require('./assets/fonts/Roboto-Medium.ttf')
    });

    // Set locale
    const { locale } = await Localization.getLocalizationAsync();
    dayjs.locale(locale.substring(0, 2)); // TODO: works for every locale?
    dayjs.extend(isSameOrBefore); // extends Plugins

    // Check for valid jwtToken and reconnect User
    const jwtToken = await AsyncStorage.getItem('jwtToken');
    if (jwtToken != null) {
      const loggedIn = await reconnect({ updateUserData });
      this.setState({ loggedIn: !!loggedIn });
    }
  };

  render() {
    const { loading, loggedIn } = this.state;
    const SwitchNavigator = createSwitchNavigator(
      { LoggedOut, LoggedIn },
      { initialRouteName: loggedIn ? 'LoggedIn' : 'LoggedOut' }
    );
    const AppContainer = createAppContainer(SwitchNavigator);
    return (
      <ApolloProvider client={Client}>
        {loading ? (
          <AppLoading
            startAsync={this.startAsync}
            onFinish={() => this.setState({ loading: false })}
            onError={console.warn}
          />
        ) : (
          <AppContainer />
        )}
      </ApolloProvider>
    );
  }
}
