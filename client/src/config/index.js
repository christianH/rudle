// To run Application on a physical Device please modify SERVER_URL;
// export const SERVER_URL = '192.168.178.31:4000';
export const SERVER_URL = 'localhost:4000';

export const FB_APP_ID = '430511617411432';
export const FB_PERMISSIONS = [
  'public_profile',
  'user_gender',
  'user_birthday',
  'user_friends',
  'user_photos'
];
