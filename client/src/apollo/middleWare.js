import { ApolloLink, Observable } from 'apollo-link';
import { AsyncStorage } from 'react-native';

const request = async operation => {
  const jwtToken = await AsyncStorage.getItem('jwtToken');
  operation.setContext({
    headers: {
      authorization: jwtToken ? `Bearer ${jwtToken}` : ''
    }
  });
};

const middleWare = new ApolloLink(
  (operation, forward) =>
    new Observable(observer => {
      let handle;
      Promise.resolve(operation)
        .then(oper => request(oper))
        .then(() => {
          handle = forward(operation).subscribe({
            next: observer.next.bind(observer),
            error: observer.error.bind(observer),
            complete: observer.complete.bind(observer)
          });
        })
        .catch(observer.error.bind(observer));
      return () => {
        if (handle) handle.unsubscribe();
      };
    })
);

export default middleWare;
