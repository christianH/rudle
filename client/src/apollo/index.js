/* eslint-disable no-shadow */
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { persistCache } from 'apollo-cache-persist';
import { withClientState } from 'apollo-link-state';
import { ApolloLink, split } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { RetryLink } from 'apollo-link-retry';
import { onError } from 'apollo-link-error';
import { getMainDefinition } from 'apollo-utilities';
import { AsyncStorage } from 'react-native';
import { SERVER_URL } from '../config';
import middleWare from './middleWare';

const httpLink = new HttpLink({
  uri: `http://${SERVER_URL}/graphql`
});

const retryLink = new RetryLink({ attempts: { max: Infinity } });

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors)
    graphQLErrors.map(({ message, path }) =>
      console.log(`[GraphQL error]: Message: ${message}, Path: ${path}`)
    );

  if (networkError) console.log(`[Network error]: ${networkError}`);
});

const wsLink = new WebSocketLink({
  uri: `ws://${SERVER_URL}/graphql`,
  options: {
    reconnect: true,
    lazy: true,
    connectionParams: async () => ({
      token: `Bearer ${await AsyncStorage.getItem('jwtToken')}`
    })
  }
});

const apiLink = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return kind === 'OperationDefinition' && operation === 'subscription';
  },
  wsLink,
  httpLink
);

const cache = new InMemoryCache({});
persistCache({
  cache,
  storage: AsyncStorage
});

const clientState = withClientState({
  defaults: {
    isConnected: true
  },
  resolvers: {
    Mutation: {
      updateNetworkStatus: (_, { isConnected }, { cache }) => {
        cache.writeData({ data: { isConnected } });
        return null;
      }
    }
  },
  cache
});

const Client = new ApolloClient({
  link: ApolloLink.from([
    clientState,
    middleWare,
    errorLink,
    retryLink,
    apiLink
  ]),
  cache,
  queryDeduplication: false
  // shouldBatch: true
});

export default Client;
