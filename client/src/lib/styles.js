import { Dimensions, Platform } from 'react-native';
import {
  getStatusBarHeight,
  getBottomSpace
} from 'react-native-iphone-x-helper';

export const windowHeight = Dimensions.get('window').height;
export const windowWidth = Dimensions.get('window').width;
export const composeHeight = Platform.OS === 'android' ? 53 : 50;
export const statusBarHeight = getStatusBarHeight();
export const bottomSpace = getBottomSpace();
export const headerHeight = Platform.OS === 'android' ? 75 : 65;
export const gutter = 16;

export const keyboardOffsetPosition =
  statusBarHeight + headerHeight - bottomSpace / 1.7;

export const keyboardOffsetPadding = -bottomSpace * 0.8;

export const flex = {
  row: {
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  left: {
    alignItems: 'flex-start'
  },
  right: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-end'
  }
};

export const colors = {
  tintColor: 'rgba(80,155,174,1)',
  tintColorLight: 'rgba(80,155,174,0.3)',
  // tintColor: 'rgba(6, 173, 182, 1)',
  // tintColorLight: 'rgba(6, 173, 182, 0.3)',
  tintColorLight2: '#EDF5F7',
  secondaryColor: '#BDBDBD',
  textColor: '#000',
  textDekorColor: '#4d4d4d',
  backgroundColor: '#FFFFFF',
  backgroundColor2: '#F7F7F7',
  borderColor: 'rgba(0, 0, 0, 0.1)',
  shadowColor: 'rgba(0, 0, 0, 0.3)'
};

export const bold = {
  ...Platform.select({
    ios: {
      fontWeight: '600'
    },
    android: {
      fontWeight: '400',
      fontFamily: 'Roboto_medium'
    }
  })
};

export const shadow = {
  shadowColor: colors.shadowColor,
  shadowOffset: { width: 0, height: 2 },
  shadowOpacity: 0.84,
  shadowRadius: 2,
  elevation: 2
};

export const noShadow = {
  shadowColor: colors.shadowColor,
  shadowOffset: { width: 0, height: 0 },
  shadowOpacity: 0,
  shadowRadius: 0,
  elevation: 0
};

export const fullscreen = {
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
  width: windowWidth,
  height: windowHeight,
  margin: 0,
  padding: 0
};

export const content = {
  marginBottom: bottomSpace,
  paddingBottom: statusBarHeight * 2
};
