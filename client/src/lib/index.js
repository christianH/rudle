import { Facebook, Location, Permissions, Notifications } from 'expo';
import { AsyncStorage } from 'react-native';
import { FB_APP_ID, FB_PERMISSIONS } from '../config';
import Client from '../apollo';
import LOGIN from '../graphql/mutations/login';
import RECONNECT from '../graphql/mutations/reconnect';
import LOGOUT from '../graphql/mutations/logout';
import UPDATE_USER_LOCATION from '../graphql/mutations/updateUserLocation';
import UPDATE_USER_PUSHTOKEN from '../graphql/mutations/updateUserPushToken';
import GET_CHATS from '../graphql/queries/getChats';
import GET_REQUESTS from '../graphql/queries/getRequests';

export async function watchUserLocation() {
  const { status } = await Permissions.askAsync(Permissions.LOCATION);
  if (status !== 'granted') {
    // this.setState({
    //   errorMessage: 'Permission to access location was denied'
    // });
  } else {
    Location.watchPositionAsync(
      { distanceInterval: 500, enableHighAccuracy: true },
      location =>
        Client.mutate({
          variables: {
            latitude: location.coords.latitude,
            longitude: location.coords.longitude
          },
          mutation: UPDATE_USER_LOCATION
        })
    );
  }
}

export async function login() {
  const { type, token } = await Facebook.logInWithReadPermissionsAsync(
    FB_APP_ID,
    {
      permissions: FB_PERMISSIONS,
      behavior: 'web' // DEV: change to web to enable switch user
    }
  );

  if (type === 'success') {
    try {
      const { data } = await Client.mutate({
        variables: { fbToken: token },
        mutation: LOGIN
      });

      await AsyncStorage.setItem('jwtToken', data.login.token);
      await watchUserLocation();
      console.log('LOGGEDIN');
      return true;
    } catch (error) {
      console.log(error);
    }
  } else {
    console.log('FACEBOOK LOGIN ERROR');
  }
  return false;
}

export async function reconnect(params) {
  try {
    if (params.updateUserData) {
      console.log('UPDATE USERDATA');
      const { data } = await Client.mutate({
        variables: {},
        mutation: RECONNECT
      });
      await AsyncStorage.setItem('jwtToken', data.reconnect.token);
    }
    await watchUserLocation();
    console.log('RECONNECTED');
    return true;
  } catch (error) {
    console.log('RECONNECT FAILED', error);
    throw error;
  }
}

export async function registerForPushNotifications() {
  const { status: existingStatus } = await Permissions.getAsync(
    Permissions.NOTIFICATIONS
  );
  let finalStatus = existingStatus;
  if (existingStatus !== 'granted') {
    const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    finalStatus = status;
  }
  if (finalStatus !== 'granted') return;
  const pushToken = await Notifications.getExpoPushTokenAsync();
  Client.mutate({
    variables: { pushToken },
    mutation: UPDATE_USER_PUSHTOKEN
  });
}

export async function handleNetworkConnectionChange(isConnected) {
  // console.log(`NetworkStatus: ${isConnected ? 'online' : 'offline'}`);
  const isLoggedIn = await AsyncStorage.getItem('jwtToken');
  if (isConnected && isLoggedIn) {
    Client.query({ query: GET_CHATS, fetchPolicy: 'network-only' });
    Client.query({ query: GET_REQUESTS, fetchPolicy: 'network-only' });
  }
}

export async function logout() {
  await Client.mutate({
    mutation: LOGOUT
  });
  await AsyncStorage.removeItem('jwtToken');
  await Client.clearStore();
  return true;
}
