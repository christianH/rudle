import gql from 'graphql-tag';

export default gql`
  subscription requestAdded {
    requestAdded {
      _id
      user {
        _id
        name
        avatar
      }
      activity {
        _id
        title
        subtitle
        image
      }
      seenBy
      createdAt
    }
  }
`;
