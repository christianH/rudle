import gql from 'graphql-tag';

export default gql`
  subscription messageAdded {
    messageAdded {
      _id
      members
      messages {
        _id
        from
        activityId
        text
        createdAt
      }
      seenBy
      lastMessageAt
      createdAt
      updatedAt
    }
  }
`;
