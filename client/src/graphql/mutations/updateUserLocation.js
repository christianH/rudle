import gql from 'graphql-tag';

export default gql`
  mutation updateUserLocation($latitude: Float!, $longitude: Float!) {
    updateUserLocation(latitude: $latitude, longitude: $longitude) {
      message
    }
  }
`;
