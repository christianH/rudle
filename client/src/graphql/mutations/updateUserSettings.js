import gql from 'graphql-tag';

export default gql`
  mutation updateUserSettings(
    $radius: Int
    $connectWithFavorites: Boolean
    $connectWithFriends: Boolean
    $connectWithFriendsOfFriends: Boolean
    $connectWithUnknown: Boolean
    $notificationRequests: Boolean
    $notificationMessages: Boolean
  ) {
    updateUserSettings(
      radius: $radius
      connectWithFavorites: $connectWithFavorites
      connectWithFriends: $connectWithFriends
      connectWithFriendsOfFriends: $connectWithFriendsOfFriends
      connectWithUnknown: $connectWithUnknown
      notificationRequests: $notificationRequests
      notificationMessages: $notificationMessages
    ) {
      message
    }
  }
`;
