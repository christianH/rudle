import gql from 'graphql-tag';

export default gql`
  mutation markRequestAsSeen($requestId: String!) {
    markRequestAsSeen(requestId: $requestId) {
      message
    }
  }
`;
