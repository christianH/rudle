import gql from 'graphql-tag';

export default gql`
  mutation sendMessage(
    $userId: String!
    $text: String!
    $activityId: String
    $createdAt: String!
  ) {
    sendMessage(
      userId: $userId
      text: $text
      activityId: $activityId
      createdAt: $createdAt
    ) {
      _id
      chatId
    }
  }
`;
