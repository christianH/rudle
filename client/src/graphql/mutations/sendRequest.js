import gql from 'graphql-tag';

export default gql`
  mutation sendRequest($activityId: String!) {
    sendRequest(activityId: $activityId) {
      _id
    }
  }
`;
