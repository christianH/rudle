import gql from 'graphql-tag';

export default gql`
  mutation login($fbToken: String!) {
    login(fbToken: $fbToken) {
      token
    }
  }
`;
