import gql from 'graphql-tag';

export default gql`
  mutation addUserToFavorites($userId: String!) {
    addUserToFavorites(userId: $userId) {
      message
    }
  }
`;
