import gql from 'graphql-tag';

export default gql`
  mutation reconnect {
    reconnect {
      token
    }
  }
`;
