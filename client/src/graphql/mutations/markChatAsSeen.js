import gql from 'graphql-tag';

export default gql`
  mutation markChatAsSeen($chatId: String!) {
    markChatAsSeen(chatId: $chatId) {
      message
    }
  }
`;
