import gql from 'graphql-tag';

export default gql`
  mutation removeUserFromFavorites($userId: String!) {
    removeUserFromFavorites(userId: $userId) {
      message
    }
  }
`;
