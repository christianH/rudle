import gql from 'graphql-tag';

export default gql`
  mutation updateUserPushToken($pushToken: String!) {
    updateUserPushToken(pushToken: $pushToken) {
      message
    }
  }
`;
