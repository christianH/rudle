import gql from 'graphql-tag';

export default gql`
  query me {
    me {
      _id
      name
      gender
      birthday
      avatar
      friends
      favorites
      location {
        latitude
        longitude
      }
      settings {
        radius
        connectWithFavorites
        connectWithFriends
        connectWithFriendsOfFriends
        connectWithUnknown
        notificationRequests
        notificationMessages
      }
      fbToken
      pushToken
      createdAt
      updatedAt
    }
  }
`;
