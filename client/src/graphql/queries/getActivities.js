import gql from 'graphql-tag';

export default gql`
  query getActivities {
    getActivities {
      _id
      title
      subtitle
      description
      image
      totalRequests
    }
  }
`;
