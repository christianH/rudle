import gql from 'graphql-tag';

export default gql`
  query getRequest($_id: ID!) {
    getRequest(_id: $_id) {
      _id
      user {
        _id
        name
        avatar
      }
      activity {
        _id
        title
        subtitle
        image
      }
      seenBy
      createdAt
    }
  }
`;
