import gql from 'graphql-tag';

export default gql`
  query getUser($_id: String!) {
    getUser(_id: $_id) {
      _id
      name
      gender
      birthday
      avatar
      about
      location {
        latitude
        longitude
      }
      pictures
      friends
      interests {
        activity {
          _id
          title
        }
        count
      }
    }
  }
`;
