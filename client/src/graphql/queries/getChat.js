import gql from 'graphql-tag';

export default gql`
  query getChat($_id: ID!) {
    getChat(_id: $_id) {
      _id
      members
      messages {
        _id
        from
        activityId
        text
        createdAt
      }
      seenBy
      lastMessageAt
      createdAt
      updatedAt
    }
  }
`;
