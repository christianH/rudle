import gql from 'graphql-tag';

export default gql`
  query getRequests {
    getRequests {
      _id
      user {
        _id
        name
        avatar
      }
      activity {
        _id
        title
        subtitle
        image
      }
      seenBy
      createdAt
    }
  }
`;
