import gql from 'graphql-tag';

export default gql`
  query notifications {
    getChats {
      _id
      seenBy
    }
    getRequests {
      _id
      seenBy
    }
    me {
      _id
    }
  }
`;
