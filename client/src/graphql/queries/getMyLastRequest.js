import gql from 'graphql-tag';

export default gql`
  query getMyLastRequest {
    getMyLastRequest {
      _id
      user {
        _id
        name
        avatar
      }
      activity {
        _id
        title
        subtitle
        image
      }
      createdAt
    }
  }
`;
