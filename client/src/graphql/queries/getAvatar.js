import gql from 'graphql-tag';

export default gql`
  query getAvatar($_id: String!) {
    getUser(_id: $_id) {
      _id
      name
      avatar
    }
  }
`;
