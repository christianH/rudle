import gql from 'graphql-tag';

export default gql`
  query getChats {
    getChats {
      _id
      members
      messages {
        _id
        from
        activityId
        text
        createdAt
      }
      seenBy
      lastMessageAt
      createdAt
      updatedAt
    }
  }
`;
