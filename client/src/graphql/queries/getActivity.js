import gql from 'graphql-tag';

export default gql`
  query getActivity($_id: ID!) {
    getActivity(_id: $_id) {
      _id
      title
      subtitle
      description
      image
      totalRequests
    }
  }
`;
