import React from 'react';
import { View, Text, Image } from 'react-native';
import { isIphoneX } from 'react-native-iphone-x-helper';
import {
  windowWidth,
  windowHeight,
  statusBarHeight,
  headerHeight
  // gutter
} from '../lib/styles';

const footer = require('../../assets/images/footer.png');

class Footer extends React.PureComponent {
  render() {
    const { topOffset } = this.props;
    return (
      <View
        style={{
          minHeight: topOffset
            ? windowHeight -
              statusBarHeight -
              headerHeight -
              topOffset -
              (isIphoneX() ? 15 : 0)
            : windowHeight / 2 - statusBarHeight - headerHeight,
          alignItems: 'center',
          justifyContent: 'center'
        }}
      >
        <View>
          <Text>Freunde einladen</Text>
        </View>

        <Image
          source={footer}
          style={{
            position: 'absolute',
            bottom: -20,
            width: windowWidth - 3
          }}
          resizeMode="contain"
        />
      </View>
    );
  }
}

export default Footer;
