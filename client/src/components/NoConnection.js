import React from 'react';
import { View, ScrollView, RefreshControl } from 'react-native';
import BackgroundWave from './BackgroundWave';
import Error from './Error';
import { colors, windowHeight, headerHeight } from '../lib/styles';

class NoConnection extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false
    };
  }

  onRefresh = () => {
    this.setState({ refreshing: true });
    setTimeout(() => {
      this.setState({ refreshing: false });
    }, 1000);
  };

  render() {
    const { refreshing } = this.state;
    return (
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            style={{ zIndex: 1 }}
            onRefresh={this.onRefresh}
            progressViewOffset={65}
            tintColor={colors.shadowColor}
          />
        }
      >
        <BackgroundWave color={colors.tintColor} />
        <View
          style={{
            height: windowHeight - headerHeight,
            justifyContent: 'center'
          }}
        >
          <Error />
        </View>
      </ScrollView>
    );
  }
}

export default NoConnection;
