import Toast from 'react-native-root-toast';
import { colors, gutter, windowWidth } from '../lib/styles';

const showToast = text => {
  Toast.show(text, {
    duration: 1500,
    position: windowWidth / 3,
    shadow: false,
    backgroundColor: colors.shadowColor,
    opacity: 1,
    containerStyle: {
      height: 44,
      borderRadius: 22,
      paddingHorizontal: gutter * 1.5,
      alignItems: 'center',
      justifyContent: 'center'
    },
    textStyle: {
      fontSize: 15
    },
    animation: true,
    hideOnPress: true,
    delay: 100
  });
};

export default showToast;
