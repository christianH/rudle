import React from 'react';
import { View, ActivityIndicator, StyleSheet } from 'react-native';
import { colors, gutter } from '../lib/styles';

const styles = StyleSheet.create({
  spinner: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: gutter
  }
});

class Spinner extends React.PureComponent {
  render() {
    return (
      <View style={styles.spinner}>
        <ActivityIndicator size="small" color={colors.secondaryColor} />
      </View>
    );
  }
}

export default Spinner;
