import React from 'react';
import Icon from '@expo/vector-icons/MaterialIcons';
import { StyleSheet, TouchableOpacity, Platform } from 'react-native';
import { withNavigation } from 'react-navigation';
import { isIphoneX } from 'react-native-iphone-x-helper';
import { gutter, colors, statusBarHeight, shadow } from '../lib/styles';

const styles = StyleSheet.create({
  closeIcon: {
    position: 'absolute',
    right: 20,
    marginTop:
      statusBarHeight +
      (isIphoneX()
        ? gutter * 2
        : Platform.OS === 'android'
        ? gutter + 10
        : gutter),
    padding: gutter / 4,
    borderRadius: 20
  }
});

class CloseButton extends React.PureComponent {
  render() {
    const { navigation, dark } = this.props;
    return (
      <TouchableOpacity
        style={[
          styles.closeIcon,
          dark
            ? { backgroundColor: colors.shadowColor }
            : { backgroundColor: colors.backgroundColor, ...shadow }
        ]}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <Icon name="close" size={20} color={dark ? '#fff' : colors.tintColor} />
      </TouchableOpacity>
    );
  }
}

export default withNavigation(CloseButton);
