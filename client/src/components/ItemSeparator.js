import React from 'react';
import { View, StyleSheet } from 'react-native';
import { colors, gutter, windowWidth } from '../lib/styles';

class ItemSeparator extends React.PureComponent {
  render() {
    const { itemSize } = this.props;
    return (
      <View
        style={{
          height: StyleSheet.hairlineWidth,
          width: windowWidth - itemSize - gutter * 2,
          backgroundColor: colors.borderColor,
          marginLeft: itemSize + gutter * 2
        }}
      />
    );
  }
}

export default ItemSeparator;
