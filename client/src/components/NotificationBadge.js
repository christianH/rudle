import React from 'react';

import { View, StyleSheet } from 'react-native';
import { withNavigation } from 'react-navigation';
import { graphql } from 'react-apollo';
import { Notifications } from 'expo';
import { registerForPushNotifications } from '../lib';
import { colors } from '../lib/styles';
import NOTIFICATIONS from '../graphql/queries/notifications';

const styles = StyleSheet.create({
  badge: {
    position: 'absolute',
    top: -2,
    right: 15,
    width: 15,
    height: 15,
    borderRadius: 10,
    backgroundColor: colors.tintColor,
    borderWidth: 2,
    borderColor: colors.backgroundColor2
  }
});

class NotificationBadge extends React.PureComponent {
  componentDidMount() {
    registerForPushNotifications();
    this.notificationSubscription = Notifications.addListener(
      this.handleNotification
    );
  }

  handleNotification = ({ origin, data }) => {
    const { navigation, data: getRequests } = this.props;
    if (origin === 'selected' && data.chatId) {
      navigation.navigate('Chat', {
        chatId: data.chatId,
        userId: data.from
      });
      return;
    }
    if (origin === 'selected' && data.requestId) {
      const startIndex = getRequests.findIndex(
        request => request._id === data.requestId
      );
      navigation.navigate('Requests', { startIndex });
    }
  };

  checkForNotifications = () => {
    const { data } = this.props;
    if (data.getChats && data.getRequests && data.me) {
      const hasNewMessages = data.getChats.find(
        chat => chat.seenBy.indexOf(data.me._id) === -1
      );
      const hasNewRequests = data.getRequests.find(
        request => request.seenBy.indexOf(data.me._id) === -1
      );
      return hasNewMessages || hasNewRequests;
    }
    return false;
  };

  render() {
    const hasNotifications = this.checkForNotifications();
    return hasNotifications ? <View style={styles.badge} /> : null;
  }
}

export default withNavigation(graphql(NOTIFICATIONS)(NotificationBadge));
