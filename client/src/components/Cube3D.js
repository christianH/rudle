import React from 'react';
import {
  PanResponder,
  Animated,
  StyleSheet,
  Platform,
  View,
  Keyboard,
  AppState
} from 'react-native';
import { isIphoneX } from 'react-native-iphone-x-helper';
import { withNavigation } from 'react-navigation';
import {
  colors,
  statusBarHeight,
  windowHeight,
  windowWidth
} from '../lib/styles';

const PERSPECTIVE = windowWidth;
const ANGLE = Math.atan(PERSPECTIVE / (windowWidth / 2));
const TR_POSITION = Platform.OS === 'ios' ? 2.44 : 2.575;
const RATIO = Platform.OS === 'ios' ? 2 : 1.2;

const progressBarHeight = 3;

const styles = StyleSheet.create({
  cubeContainer: {
    position: 'absolute'
  },
  cubeItems: {
    backgroundColor: '#000',
    position: 'absolute',
    width: windowWidth,
    height: windowHeight
  },
  progressBarContainer: {
    position: 'absolute',
    top: 0,
    width: isIphoneX() ? windowWidth / 2 : windowWidth,
    marginTop: isIphoneX() ? statusBarHeight + progressBarHeight : 0,
    marginLeft: isIphoneX() ? windowWidth / 4 : 0,
    backgroundColor: 'transparent'
  },
  progressBarProgress: {
    position: 'absolute',
    top: 0,
    height: progressBarHeight,
    borderRadius: progressBarHeight / 2,
    backgroundColor: colors.shadowColor
  }
});

class Cube3D extends React.PureComponent {
  constructor(props) {
    super(props);
    const { children, onEndReached } = this.props;
    this.cube = new Animated.ValueXY({ x: 0, y: 0 });
    this.pages = children.map((_, i) => windowWidth * -i);
    this.progressBar = children.map(() => new Animated.Value(0));
    this.currentPosition = { x: 0, y: 0 };

    this.panResponder = PanResponder.create({
      onMoveShouldSetResponderCapture: (e, gestureState) =>
        Math.abs(gestureState.dx) > 30,

      onMoveShouldSetPanResponderCapture: (e, gestureState) =>
        Math.abs(gestureState.dx) > 30,

      onPanResponderGrant: () => {
        this.stopAutoplay();
        this.cube.stopAnimation();
        this.cube.setOffset({
          x: this.currentPosition.x,
          y: this.currentPosition.y
        });
      },

      onPanResponderMove: (e, gestureState) => {
        Animated.event([null, { dx: this.cube.x }])(e, gestureState);
      },

      onPanResponderRelease: (e, gestureState) => {
        const mod = gestureState.dx > 0 ? 150 : -150;
        const index = this.getClosest(this.currentPosition.x + mod);

        this.cube.flattenOffset({
          x: this.currentPosition.x,
          y: this.currentPosition.y
        });

        if (
          this.currentPosition.x > this.pages[0] ||
          this.currentPosition.x < this.pages[this.pages.length - 1]
        ) {
          this.scrollTo(index, false);
          onEndReached();
        } else {
          this.scrollTo(index, true);
        }
      }
    });
  }

  componentDidMount() {
    const { startIndex, navigation, onEndReached } = this.props;

    AppState.addEventListener('change', this.handleAppStateChange);

    this.componentWillFocus = navigation.addListener('willFocus', () => {
      const index = this.getClosest(this.currentPosition.x);
      this.scrollTo(index || startIndex, false);
    });

    this.componentWillBlur = navigation.addListener('willBlur', () => {
      this.stopAutoplay();
    });

    this.keyboardDidShow = Keyboard.addListener('keyboardDidShow', () => {
      this.stopAutoplay();
    });

    this.keyboardDidHide = Keyboard.addListener('keyboardDidHide', () => {
      const index = this.getClosest(this.currentPosition.x);
      this.startAutoplay(index);
    });

    this.cube.addListener(value => {
      this.currentPosition = value;
    });

    this.progressBar.forEach((_, i) => {
      this.progressBar[i].addListener(({ value }) => {
        if (value === 1) {
          if (i + 1 < this.pages.length) this.scrollTo(i + 1, true);
          else onEndReached();
        }
      });
    });
  }

  componentWillUnmount() {
    this.stopAutoplay();
    this.cube.stopAnimation();
    this.componentWillFocus.remove();
    this.componentWillBlur.remove();
    this.keyboardDidShow.remove();
    this.keyboardDidHide.remove();
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  handleAppStateChange = nextAppState => {
    if (nextAppState === 'active') {
      const index = this.getClosest(this.currentPosition.x);
      this.startAutoplay(index);
    }
    if (nextAppState === 'inactive' || nextAppState === 'background') {
      this.stopAutoplay();
    }
  };

  scrollTo = (index, animated = false) => {
    if (animated) {
      Animated.spring(this.cube, {
        toValue: { x: this.pages[index], y: 0 },
        friction: 10,
        tension: 1
      }).start();
    } else {
      this.cube.setValue({ x: this.pages[index], y: 0 });
    }
    this.startAutoplay(index);
    const { onScrollCallback } = this.props;
    onScrollCallback(index);
  };

  startAutoplay = index => {
    const { autoplayInterval } = this.props;
    // eslint-disable-next-line no-underscore-dangle
    const progress = this.progressBar[index].__getValue();
    const duration = autoplayInterval - autoplayInterval * progress;
    this.progressBar.forEach((_, i) => {
      if (i !== index) this.progressBar[i].setValue(0);
    });
    Animated.timing(this.progressBar[index], {
      toValue: 1,
      duration
    }).start();
  };

  stopAutoplay = () => {
    this.progressBar.forEach((_, i) => {
      Animated.timing(this.progressBar[i]).stop();
    });
  };

  getClosest = num => {
    let minDiff = 1000;
    let ans;

    this.pages.forEach(element => {
      const m = Math.abs(num - element);
      if (m < minDiff) {
        minDiff = m;
        ans = element;
      }
    });
    return this.pages.indexOf(ans);
  };

  getCubeStyles = index => {
    const scrollX = this.cube.x;
    const pageX = -windowWidth * index;

    const translateX = scrollX.interpolate({
      inputRange: [pageX - windowWidth, pageX, pageX + windowWidth],
      outputRange: [(-windowWidth - 1) / RATIO, 0, (windowWidth + 1) / RATIO],
      extrapolate: 'clamp'
    });

    const rotateY = scrollX.interpolate({
      inputRange: [pageX - windowWidth, pageX + windowWidth],
      outputRange: [`-${ANGLE}rad`, `${ANGLE}rad`],
      extrapolate: 'clamp'
    });

    const translateXAfterRotate = scrollX.interpolate({
      inputRange: [
        pageX - windowWidth,
        pageX - windowWidth + 0.01,
        pageX,
        pageX + windowWidth - 0.01,
        pageX + windowWidth
      ],
      outputRange: [
        -windowWidth - 1,
        (-windowWidth - 1) / TR_POSITION,
        0,
        (windowWidth + 1) / TR_POSITION,
        +windowWidth + 1
      ],
      extrapolate: 'clamp'
    });

    const opacity = scrollX.interpolate({
      inputRange: [
        pageX - windowWidth,
        pageX - windowWidth + 10,
        pageX,
        pageX + windowWidth - 150,
        pageX + windowWidth
      ],
      outputRange: [0.2, 0.6, 1, 0.6, 0.2],
      extrapolate: 'clamp'
    });

    return {
      transform: [
        { perspective: PERSPECTIVE },
        { translateX },
        { rotateY },
        { translateX: translateXAfterRotate }
      ],
      opacity
    };
  };

  renderProgressBar = index => {
    const widthInterpolated = this.progressBar[index].interpolate({
      inputRange: [0, 1],
      outputRange: ['0%', '100%'],
      extrapolate: 'clamp'
    });

    return (
      <View style={styles.progressBarContainer}>
        <Animated.View
          style={[styles.progressBarProgress, { width: widthInterpolated }]}
        />
      </View>
    );
  };

  renderChild = (child, index) => (
    <Animated.View
      style={[
        StyleSheet.absoluteFill,
        { backgroundColor: 'transparent' },
        this.getCubeStyles(index, false)
      ]}
      key={`child- ${index}`}
    >
      {child}
      {this.renderProgressBar(index)}
    </Animated.View>
  );

  render() {
    const { children } = this.props;

    return (
      <View style={styles.cubeContainer} {...this.panResponder.panHandlers}>
        <View style={styles.cubeItems}>{children.map(this.renderChild)}</View>
      </View>
    );
  }
}

export default withNavigation(Cube3D);
