/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { StyleSheet, Platform, Switch } from 'react-native';
import { gutter, colors } from '../lib/styles';

const styles = StyleSheet.create({
  switch: {
    marginRight: gutter / 2
  }
});

class MySwitch extends React.PureComponent {
  render() {
    return (
      <Switch
        trackColor={{
          true:
            Platform.OS === 'android' ? colors.tintColorLight : colors.tintColor
        }}
        thumbColor={
          Platform.OS === 'android'
            ? this.props.value
              ? colors.tintColor
              : '#fff'
            : null
        }
        style={styles.switch}
        {...this.props}
      />
    );
  }
}

export default MySwitch;
