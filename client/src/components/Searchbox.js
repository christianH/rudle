import React from 'react';
import { View, TextInput, StyleSheet } from 'react-native';
import Icon from '@expo/vector-icons/MaterialIcons';
import { colors, gutter, windowWidth, shadow } from '../lib/styles';

const styles = StyleSheet.create({
  searchWrapper: {
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    width: windowWidth - gutter * 7.5,
    height: 44,
    backgroundColor: colors.backgroundColor,
    borderRadius: 22,
    ...shadow,
    margin: gutter
  },
  searchIcon: {
    paddingRight: 15
  },
  input: {
    flex: 1,
    height: 44,
    paddingLeft: gutter,
    fontSize: 15
  }
});

class SearchBox extends React.PureComponent {
  state = {
    searchText: null
  };

  clearSearchText() {
    this.setState({ searchText: null });
  }

  render() {
    const { placeholder } = this.props;
    const { searchText } = this.state;
    return (
      <View style={styles.searchWrapper}>
        <TextInput
          value={searchText}
          onChangeText={text => {
            this.setState({ searchText: text });
          }}
          style={styles.input}
          placeholder={placeholder}
          placeholderTextColor={colors.secondaryColor}
          tintColor={colors.tintColor}
          selectionColor={colors.tintColor}
          underlineColorAndroid="transparent"
        />

        {searchText ? (
          <Icon
            style={styles.searchIcon}
            name="close"
            size={20}
            color={colors.secondaryColor}
            onPress={() => this.clearSearchText()}
          />
        ) : (
          <Icon
            style={styles.searchIcon}
            name="search"
            size={20}
            color={colors.secondaryColor}
          />
        )}
      </View>
    );
  }
}

export default SearchBox;
