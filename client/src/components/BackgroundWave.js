import React from 'react';
import { View, Platform } from 'react-native';
import { windowWidth, windowHeight } from '../lib/styles';

const androidFix = Platform.OS === 'android' ? 5 : 0;

class BackgroundWave extends React.PureComponent {
  render() {
    const { color, inverted } = this.props;
    return (
      <View
        style={{
          zIndex: -1,
          position: 'absolute',
          left: 0,
          width: windowWidth,
          borderRadius: windowWidth / 2,
          backgroundColor: color,
          transform: [{ scaleX: 1.63 }],
          height: windowHeight,
          top: inverted ? 26.4 - androidFix : 105 + androidFix - windowHeight
        }}
      />
    );
  }
}

export default BackgroundWave;
