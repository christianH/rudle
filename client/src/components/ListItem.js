import React from 'react';
import { View, Text, StyleSheet, TouchableHighlight } from 'react-native';
import Icon from '@expo/vector-icons/MaterialIcons';
import { gutter, colors, flex } from '../lib/styles';

const styles = StyleSheet.create({
  listItem: {
    marginLeft: gutter * 2 + 20,
    paddingTop: gutter / 2,
    paddingBottom: gutter / 2,
    paddingRight: gutter / 2,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.borderColor
  },
  listItemText: {
    fontSize: 15,
    alignSelf: 'center',
    color: colors.textDekorColor
  },
  chevron: {
    padding: gutter / 4
  }
});

class ListItem extends React.PureComponent {
  render() {
    const { text, onPress, children } = this.props;
    return (
      <TouchableHighlight
        onPress={onPress}
        underlayColor="lightgray"
        style={{ marginLeft: -20 }}
      >
        <View style={styles.listItem}>
          <View style={flex.row}>
            <Text style={styles.listItemText}>{text}</Text>
            <View style={flex.right}>
              {children || (
                <Icon
                  name="chevron-right"
                  size={24}
                  color={colors.secondaryColor}
                  style={styles.chevron}
                />
              )}
            </View>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}

export default ListItem;
