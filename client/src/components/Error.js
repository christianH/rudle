import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Icon from '@expo/vector-icons/MaterialIcons';
import { colors, gutter } from '../lib/styles';

const styles = StyleSheet.create({
  error: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: gutter
  }
});

class Error extends React.PureComponent {
  render() {
    return (
      <View style={styles.error}>
        <Icon size={24} color={colors.shadowColor} name="error-outline" />
        <Text>No Connection</Text>
      </View>
    );
  }
}

export default Error;
