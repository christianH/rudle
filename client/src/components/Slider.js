import React from 'react';
import { View, StyleSheet, Platform } from 'react-native';
import Slider from 'react-native-slider';
import { gutter, colors, bold, shadow } from '../lib/styles';

const styles = StyleSheet.create({
  sliderContainer: {
    alignItems: 'stretch',
    justifyContent: 'center',
    paddingLeft: gutter * 2,
    paddingRight: gutter * 2 + 20,
    paddingTop: gutter / 3,
    paddingBottom: gutter / 4
  },
  thumbStyle: {
    height: Platform.OS === 'android' ? 20 : 26,
    width: Platform.OS === 'android' ? 20 : 26,
    borderRadius: Platform.OS === 'android' ? 10 : 13,
    ...shadow
  },
  sliderValue: {
    ...bold,
    color: colors.tintColor,
    marginRight: 20
  }
});

class MySlider extends React.PureComponent {
  render() {
    return (
      <View>
        <View style={styles.sliderContainer}>
          <Slider
            minimumValue={1}
            maximumValue={200}
            step={1}
            thumbTintColor={
              Platform.OS === 'android' ? colors.tintColor : '#fff'
            }
            thumbStyle={styles.thumbStyle}
            minimumTrackTintColor={colors.tintColor}
            maximumTrackTintColor={colors.secondaryColor}
            {...this.props}
          />
        </View>
      </View>
    );
  }
}

export default MySlider;
