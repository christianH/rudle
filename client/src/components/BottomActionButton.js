import React from 'react';
import { Button } from 'react-native-elements';
import { StyleSheet } from 'react-native';
import { LinearGradient } from 'expo';
import { gutter, bottomSpace, shadow, colors, bold } from '../lib/styles';

const styles = StyleSheet.create({
  gradient: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center'
  },
  container: {
    marginTop: 30,
    marginBottom: gutter + bottomSpace / 3
  },
  button: {
    minWidth: 100,
    height: 40,
    paddingHorizontal: gutter,
    borderRadius: 20,
    backgroundColor: colors.backgroundColor,
    ...shadow
  },
  title: {
    fontSize: 15,
    ...bold,
    color: colors.tintColor
  }
});

class BottomActionButton extends React.PureComponent {
  render() {
    const { title, onPress } = this.props;
    return (
      <LinearGradient
        start={[0, 0]}
        end={[0, 1]}
        colors={['rgba(255,255,255,0)', 'rgba(255,255,255,1)']}
        style={styles.gradient}
      >
        <Button
          title={title}
          titleStyle={styles.title}
          buttonStyle={styles.button}
          containerStyle={styles.container}
          onPress={onPress}
        />
      </LinearGradient>
    );
  }
}

export default BottomActionButton;
