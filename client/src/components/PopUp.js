import React from 'react';
import { View, ScrollView, StyleSheet } from 'react-native';
import { withNavigation } from 'react-navigation';

import {
  statusBarHeight,
  windowWidth,
  content,
  colors,
  windowHeight
} from '../lib/styles';
import CloseButton from './CloseButton';

const styles = StyleSheet.create({
  statusBarBackground: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: windowWidth,
    height: statusBarHeight,
    backgroundColor: colors.backgroundColor
  }
});

class PopUp extends React.PureComponent {
  state = {
    statusBarSolid: false
  };

  closePopUp = event => {
    const { y } = event.nativeEvent.contentOffset;
    const { navigation } = this.props;
    if (y <= 0) {
      navigation.goBack();
    }
  };

  handleScroll = event => {
    const { statusBarSwitchToSolid } = this.props;
    if (statusBarSwitchToSolid) {
      const { y } = event.nativeEvent.contentOffset;
      if (y >= statusBarSwitchToSolid) {
        this.setState({ statusBarSolid: true });
      } else {
        this.setState({ statusBarSolid: false });
      }
    }
  };

  render() {
    const { children } = this.props;
    const { statusBarSolid } = this.state;
    return (
      <View>
        <ScrollView
          contentContainerStyle={{ ...content, minHeight: windowHeight }}
          onScrollEndDrag={this.closePopUp}
          onScroll={this.handleScroll}
          scrollEventThrottle={16}
          bounces={false}
        >
          {children}
        </ScrollView>
        {statusBarSolid ? <View style={styles.statusBarBackground} /> : null}
        <CloseButton dark />
      </View>
    );
  }
}

export default withNavigation(PopUp);
