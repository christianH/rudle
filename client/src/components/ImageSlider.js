import React from 'react';
import { View, Image, StyleSheet } from 'react-native';
import Swiper from 'react-native-swiper';
import { colors } from '../lib/styles';

const styles = StyleSheet.create({
  image: {
    flex: 1
  }
});

class ImageSlider extends React.PureComponent {
  render() {
    const { images, width, height } = this.props;
    return (
      <View style={{ width, height }}>
        <Swiper
          showsButtons={false}
          loop={false}
          dotColor={colors.shadowColor}
          activeDotColor="#fff"
        >
          {images.map(item => (
            <Image key={item} source={{ uri: item }} style={styles.image} />
          ))}
        </Swiper>
      </View>
    );
  }
}

export default ImageSlider;
