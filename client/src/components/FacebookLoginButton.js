import React from 'react';
import { StyleSheet, TouchableNativeFeedback } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Button } from 'react-native-elements';
import Icon from '@expo/vector-icons/FontAwesome';
import { login } from '../lib';
import { gutter, bold } from '../lib/styles';

const styles = StyleSheet.create({
  container: {
    margin: gutter * 2.5
  },
  title: {
    color: '#000',
    fontSize: 15,
    ...bold,
    marginLeft: gutter / 2
  },
  button: {
    alignSelf: 'center',
    paddingHorizontal: gutter,
    borderWidth: 1,
    borderColor: '#000',
    backgroundColor: 'transparent',
    borderRadius: 22
  }
});

class FacebookLoginButton extends React.PureComponent {
  render() {
    const { navigation } = this.props;
    return (
      <Button
        clear
        title="Mit Facebook anmelden"
        titleStyle={styles.title}
        buttonStyle={styles.button}
        containerStyle={styles.container}
        background={TouchableNativeFeedback.SelectableBackground()} // Hack to use borderRadius
        icon={<Icon name="facebook" size={15} color="#000" />}
        onPress={() => {
          login().then(loggedIn =>
            loggedIn ? navigation.navigate('LoggedIn') : null
          );
        }}
      />
    );
  }
}

export default withNavigation(FacebookLoginButton);
