import { createMaterialTopTabNavigator } from 'react-navigation';

import Messenger from './Messenger';
import Activities from './Activities';
import Account from './Account';

export default createMaterialTopTabNavigator(
  {
    Account,
    Activities,
    Messenger
  },
  {
    initialRouteName: 'Activities',
    tabBarOptions: {
      style: { height: 0 }
    }
  }
);
