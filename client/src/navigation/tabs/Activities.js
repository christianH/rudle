import React from 'react';
import { View, TouchableWithoutFeedback } from 'react-native';
import Icon from '@expo/vector-icons/MaterialIcons';
import IconC from '@expo/vector-icons/MaterialCommunityIcons';
import { createStackNavigator } from 'react-navigation';

import Activities from '../../screens/Activities';
import { colors, headerHeight } from '../../lib/styles';
import NotificationBadge from '../../components/NotificationBadge';

export default createStackNavigator(
  { Activities },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitle: 'rudle',
      headerStyle: {
        height: headerHeight,
        backgroundColor: colors.backgroundColor2,
        borderBottomWidth: 0,
        elevation: 0
      },
      headerTitleAllowFontScaling: false,
      headerTintColor: 'black',
      headerTitleStyle: {
        flex: 1,
        fontSize: 50,
        fontFamily: 'SignPainter',
        fontWeight: '200',
        textAlign: 'center',
        paddingTop: 4
      },
      headerLeft: (
        <IconC
          size={29}
          name="account-outline"
          style={{ paddingLeft: 20 }}
          onPress={() => navigation.navigate('Account')}
        />
      ),
      headerRight: (
        <TouchableWithoutFeedback
          onPress={() => navigation.navigate('Messenger')}
        >
          <View>
            <Icon
              size={24}
              name="chat-bubble-outline"
              style={{
                paddingRight: 20,
                paddingTop: 4
              }}
            />
            <NotificationBadge />
          </View>
        </TouchableWithoutFeedback>
      )
    }),
    cardStyle: { backgroundColor: colors.backgroundColor2 }
  }
);
