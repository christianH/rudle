import React from 'react';
import Icon from '@expo/vector-icons/MaterialCommunityIcons';
import { createStackNavigator } from 'react-navigation';

import Messenger from '../../screens/Messenger';
import { colors, gutter, headerHeight, bold } from '../../lib/styles';

export default createStackNavigator(
  { Messenger },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitle: 'Messenger',
      headerLeft: (
        <Icon
          size={28}
          name="keyboard-backspace"
          style={{ paddingLeft: gutter }}
          onPress={() => navigation.navigate('Activities')}
        />
      ),
      headerRight: (
        <Icon
          size={28}
          name="account-plus-outline"
          style={{ paddingRight: gutter }}
          // onPress={() => navigation.navigate('Requests', { startIndex: 0 })}
        />
      ),
      headerStyle: {
        height: headerHeight,
        backgroundColor: colors.tintColor,
        borderBottomWidth: 0,
        elevation: 0
      },
      headerTitleAllowFontScaling: false,
      headerTintColor: 'black',
      headerTitleStyle: {
        fontSize: 24,
        ...bold,
        textAlign: 'center',
        flex: 1
      }
    }),
    cardStyle: { backgroundColor: colors.backgroundColor }
  }
);
