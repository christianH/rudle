import React from 'react';
import Icon from '@expo/vector-icons/MaterialIcons';
import { createStackNavigator } from 'react-navigation';

import Account from '../../screens/Account';
import { logout } from '../../lib';
import { colors, gutter, headerHeight, bold } from '../../lib/styles';

export default createStackNavigator(
  { Account },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitle: 'Account',
      headerLeft: (
        <Icon
          size={30}
          name="lock-outline"
          style={{ paddingLeft: gutter }}
          onPress={() => {
            logout().then(loggedOut =>
              loggedOut ? navigation.navigate('LoggedOut') : null
            );
          }}
        />
      ),
      headerRight: (
        <Icon
          size={26}
          name="keyboard-backspace"
          style={{
            transform: [{ rotate: '180deg' }],
            paddingLeft: gutter
          }}
          onPress={() => navigation.navigate('Activities')}
        />
      ),
      headerStyle: {
        height: headerHeight,
        backgroundColor: colors.tintColor,
        borderBottomWidth: 0,
        elevation: 0
      },
      headerTitleAllowFontScaling: false,
      headerTintColor: 'black',
      headerTitleStyle: {
        fontSize: 24,
        ...bold,
        textAlign: 'center',
        flex: 1
      }
    }),
    cardStyle: { backgroundColor: colors.backgroundColor }
  }
);
