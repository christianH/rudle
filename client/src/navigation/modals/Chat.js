import React from 'react';
import Icon from '@expo/vector-icons/MaterialIcons';
import { StyleSheet, View, Text, Platform } from 'react-native';
import { Avatar } from 'react-native-elements';
import { Query } from 'react-apollo';
import { createStackNavigator } from 'react-navigation';

import Chat from '../../screens/Chat';
import Spinner from '../../components/Spinner';
import Error from '../../components/Error';
import GET_AVATAR from '../../graphql/queries/getAvatar';

import { colors, gutter, shadow, headerHeight, bold } from '../../lib/styles';

export default createStackNavigator(
  { Chat },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      headerTitle: (
        <Query
          query={GET_AVATAR}
          variables={{ _id: navigation.state.params.userId }}
        >
          {({ loading, error, data }) => {
            if (loading) return <Spinner />;
            if (error) return <Error />;
            return (
              <View
                style={{
                  flex: 1,
                  alignSelf: 'center',
                  alignItems: 'center',
                  paddingTop: Platform.OS === 'android' ? 6 : 0
                }}
              >
                <Avatar
                  size={42}
                  rounded
                  source={{ uri: data.getUser.avatar }}
                  containerStyle={{ ...shadow }}
                  onPress={() =>
                    navigation.navigate('User', {
                      userId: data.getUser._id
                    })
                  }
                />
                <Text
                  style={{
                    fontSize: 10,
                    marginTop: Platform.OS === 'android' ? 1 : 3,
                    ...bold
                  }}
                >
                  {data.getUser.name}
                </Text>
              </View>
            );
          }}
        </Query>
      ),
      headerLeft: (
        <Icon
          size={26}
          name="keyboard-backspace"
          style={{ paddingLeft: gutter }}
          onPress={() => navigation.navigate('Messenger')}
        />
      ),
      headerRight: (
        <Icon
          size={24}
          name="more-vert"
          style={{ paddingRight: gutter }}
          // TODO: Action?
          // onPress={() => ???)}
        />
      ),
      headerStyle: {
        height: headerHeight,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: colors.secondaryColor,
        elevation: 0
      },
      headerTitleAllowFontScaling: false,
      headerTintColor: 'black',
      headerTitleStyle: {
        fontSize: 24,
        ...bold,
        textAlign: 'center',
        flex: 1
      }
    })
  }
);
