import { createStackNavigator } from 'react-navigation';

import TabNavigator from './tabs';
import Welcome from '../screens/Welcome';
import Activity from '../screens/Activity';
import User from '../screens/User';
import Requests from '../screens/Requests';
import Chat from './modals/Chat';

import { colors } from '../lib/styles';

export const LoggedOut = createStackNavigator(
  { Welcome },
  { headerMode: 'none' }
);

export const LoggedIn = createStackNavigator(
  {
    TabNavigator,
    Activity, // Modal
    User, // Modal
    Requests, // Modal
    Chat // Modal
  },
  {
    headerMode: 'none',
    initialRouteName: 'TabNavigator',
    mode: 'modal',
    cardStyle: { backgroundColor: colors.backgroundColor }
  }
);
