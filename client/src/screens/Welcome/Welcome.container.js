import React from 'react';
import { View, Text, ImageBackground, StyleSheet } from 'react-native';

import FacebookLoginButton from '../../components/FacebookLoginButton';
import { fullscreen, gutter } from '../../lib/styles';

const styles = StyleSheet.create({
  logoContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: gutter * 2
  },
  logoText: {
    color: '#000',
    fontFamily: 'SignPainter',
    fontSize: 100
  },
  infoText: {
    fontSize: 15,
    color: '#000'
  },
  dots: {
    fontSize: 15,
    color: '#000'
  }
});

const background = require('../../../assets/images/background.png');

class Welcome extends React.PureComponent {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          style={{ ...fullscreen }}
          imageStyle={{ ...fullscreen }}
          source={background}
        />
        <View style={styles.logoContainer}>
          <Text style={styles.logoText}> rudle </Text>
          <Text style={styles.infoText}>
            Verabrede dich mit Leuten aus deinem Rudel
          </Text>
          <Text style={styles.dots}>. . . </Text>
        </View>

        <FacebookLoginButton />
      </View>
    );
  }
}

export default Welcome;
