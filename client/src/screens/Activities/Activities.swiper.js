import React from 'react';
import {
  Text,
  View,
  ImageBackground,
  TouchableWithoutFeedback,
  StyleSheet
} from 'react-native';
import { withNavigation } from 'react-navigation';
import Carousel from 'react-native-snap-carousel';

import {
  gutter,
  windowWidth,
  colors,
  shadow,
  windowHeight,
  bold
} from '../../lib/styles';

const slideWidth = windowWidth - gutter * 5;
const shadowWidth = gutter / 2;
const itemWidth = slideWidth + shadowWidth;

const styles = StyleSheet.create({
  sliderContainer: {
    flex: 1,
    alignItems: 'center'
  },
  slide: {
    flex: 1,
    alignSelf: 'center',
    width: slideWidth,
    borderRadius: gutter / 2,
    marginTop: gutter * 1.5,
    marginBottom: gutter, // needed for shadow
    ...shadow
  },
  slideBackground: {
    flex: 1,
    borderRadius: gutter / 2
  },
  cardTitleContainer: {
    marginTop: windowHeight * 0.15,
    alignItems: 'center'
  },
  cardTitle: {
    fontSize: 24,
    ...bold
  },
  cardSubtitle: {
    fontSize: 15
  },
  dots: {
    fontSize: 24,
    lineHeight: gutter,
    alignSelf: 'center',
    color: colors.secondaryColor
  }
});

class ActivitySwiper extends React.PureComponent {
  renderItem = ({ item }) => {
    const { navigation } = this.props;
    return (
      <TouchableWithoutFeedback
        onPress={() =>
          navigation.navigate('Activity', { activityId: item._id })
        }
      >
        <View style={styles.slide}>
          <ImageBackground
            style={styles.slideBackground}
            imageStyle={styles.slideBackground}
            source={{ uri: item.image }}
          >
            <View style={styles.cardTitleContainer}>
              <Text style={styles.cardTitle}>{item.title}</Text>
              <Text style={styles.cardSubtitle}>{item.subtitle}</Text>
            </View>
          </ImageBackground>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  render() {
    const { activities } = this.props;
    return (
      <View style={styles.sliderContainer}>
        <Carousel
          data={activities}
          renderItem={this.renderItem}
          sliderWidth={windowWidth}
          itemWidth={itemWidth}
          loop
          initialNumToRender={3}
          maxToRenderPerBatch={20}
        />
        <Text style={styles.dots}>. . .</Text>
      </View>
    );
  }
}

export default withNavigation(ActivitySwiper);
