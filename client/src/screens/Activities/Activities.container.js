import React from 'react';
import { View } from 'react-native';
import { Query } from 'react-apollo';
import Spinner from '../../components/Spinner';
import Error from '../../components/Error';
import GET_ACTIVITIES from '../../graphql/queries/getActivities';
import BackgroundWave from '../../components/BackgroundWave';
import SearchBox from '../../components/Searchbox';
import ActivitySwiper from './Activities.swiper';
import { content, colors } from '../../lib/styles';

class Activities extends React.PureComponent {
  render() {
    return (
      <View style={{ ...content, flex: 1 }}>
        <BackgroundWave inverted color={colors.backgroundColor} />
        <SearchBox placeholder="Was willst du unternehmen?" />
        <Query query={GET_ACTIVITIES}>
          {({ loading, error, data }) => {
            if (loading) return <Spinner />;
            if (error) return <Error />;
            return <ActivitySwiper activities={data.getActivities} />;
          }}
        </Query>
      </View>
    );
  }
}

export default Activities;
