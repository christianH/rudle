import React from 'react';
import { Text, View, StyleSheet, Platform } from 'react-native';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import { isIphoneX } from 'react-native-iphone-x-helper';
import { statusBarHeight, flex, gutter, bold } from '../../../lib/styles';

dayjs.extend(relativeTime);

const styles = StyleSheet.create({
  requestHeader: {
    position: 'absolute',
    top: 0,
    marginTop:
      statusBarHeight +
      (isIphoneX()
        ? gutter * 2
        : Platform.OS === 'android'
        ? gutter + 10
        : gutter),
    marginLeft: gutter
  },
  userInfo: {
    justifyContent: 'center',
    marginTop: -gutter / 4,
    marginLeft: gutter / 4
  },
  listItemTitle: {
    fontSize: 15,
    lineHeight: 20,
    ...bold
  },
  listItemSubTitle: {
    fontSize: 13,
    lineHeight: 15
  }
});

class RequestHeader extends React.PureComponent {
  render() {
    const { request } = this.props;
    const fromNow = dayjs(request.createdAt).fromNow();
    return (
      <View style={styles.requestHeader}>
        <View style={flex.row}>
          <View style={styles.userInfo}>
            <Text style={styles.listItemTitle}>{request.user.name}</Text>
            <Text style={styles.listItemSubTitle}>{fromNow}</Text>
          </View>
        </View>
      </View>
    );
  }
}

export default RequestHeader;
