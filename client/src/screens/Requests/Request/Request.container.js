import React from 'react';
import { Image, KeyboardAvoidingView } from 'react-native';

import RequestHeader from './Request.header';
import RequestContent from './Request.content';
import RequestCompose from './Request.compose';
import CloseButton from '../../../components/CloseButton';

import { keyboardOffsetPadding, fullscreen } from '../../../lib/styles';

class Request extends React.PureComponent {
  render() {
    const { request, me } = this.props;
    return (
      <KeyboardAvoidingView
        enabled
        behavior="padding"
        keyboardVerticalOffset={keyboardOffsetPadding}
        style={{ flex: 1 }}
        key={request._id}
      >
        <Image
          source={{ uri: request.activity.image }}
          style={{ ...fullscreen }}
          blurRadius={20}
        />
        <RequestHeader request={request} />
        <RequestContent request={request} />
        <RequestCompose
          request={request}
          placeholder="Nachricht senden"
          me={me}
        />
        <CloseButton dark />
      </KeyboardAvoidingView>
    );
  }
}

export default Request;
