import React from 'react';
import Icon from '@expo/vector-icons/MaterialIcons';
import { View, TextInput, StyleSheet, Platform, Keyboard } from 'react-native';
import { withNavigation } from 'react-navigation';
import { isIphoneX } from 'react-native-iphone-x-helper';
import uuidV4 from 'uuid/v4';
import { Mutation } from 'react-apollo';
import Client from '../../../apollo';
import SEND_MESSAGE from '../../../graphql/mutations/sendMessage';
import GET_CHATS from '../../../graphql/queries/getChats';
import GET_AVATAR from '../../../graphql/queries/getAvatar';
import showToast from '../../../components/Toast';
import { colors, windowWidth, gutter, bottomSpace } from '../../../lib/styles';

const styles = StyleSheet.create({
  composeContainer: {
    alignSelf: 'flex-end',
    width: windowWidth,
    paddingBottom: isIphoneX() ? bottomSpace : gutter / 2
  },
  composeWrapper: {
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'flex-end',
    width: windowWidth - gutter * 4.5,
    minHeight: 44,
    borderRadius: 22,
    backgroundColor: colors.shadowColor,
    marginVertical: gutter / 2,
    paddingHorizontal: gutter / 2,
    paddingBottom: 12
  },
  composeIcon: {
    paddingRight: gutter - 4,
    paddingLeft: gutter,
    marginBottom: -4
  },
  textInput: {
    flex: 1,
    paddingLeft: gutter,
    fontSize: 15,
    color: colors.backgroundColor,
    marginBottom: Platform.OS === 'android' ? -4 : 0
  }
});

class RequestCompose extends React.PureComponent {
  state = {
    msgText: null
  };

  render() {
    const { msgText } = this.state;
    const { placeholder, request, me } = this.props;
    return (
      <View style={styles.composeContainer}>
        <View style={styles.composeWrapper}>
          <TextInput
            style={styles.textInput}
            onChangeText={newMsgText => this.setState({ msgText: newMsgText })}
            value={msgText}
            placeholder={placeholder}
            placeholderTextColor={colors.backgroundColor}
            selectionColor={colors.tintColor}
            tintColor={colors.tintColor}
            underlineColorAndroid="transparent"
            multiline
            maxHeight={66}
          />
          <Mutation mutation={SEND_MESSAGE}>
            {sendMessage => (
              <Icon
                style={styles.composeIcon}
                name="send"
                size={24}
                color={colors.backgroundColor}
                onPress={async () => {
                  if (msgText) {
                    const timeStamp = new Date();
                    // TODO: (hacky solution) prefetch query to prevent error
                    await Client.query({
                      query: GET_AVATAR,
                      variables: { _id: request.user._id }
                    });
                    sendMessage({
                      variables: {
                        userId: request.user._id,
                        text: msgText.trim(),
                        activityId: request.activity._id,
                        createdAt: timeStamp
                      },
                      optimisticResponse: {
                        __typename: 'Mutation',
                        sendMessage: {
                          __typename: 'Message',
                          _id: uuidV4(),
                          chatId: uuidV4()
                        }
                      },
                      // eslint-disable-next-line no-shadow
                      update: (cache, { data: { sendMessage } }) => {
                        const prev = cache.readQuery({ query: GET_CHATS });
                        const next = prev;

                        const index = prev.getChats.findIndex(
                          chatObj =>
                            chatObj.members.includes(me._id) &&
                            chatObj.members.includes(request.user._id)
                        );

                        if (index === -1) {
                          const chatObj = {
                            __typename: 'Chat',
                            _id: sendMessage.chatId,
                            members: [me._id, request.user._id],
                            messages: [
                              {
                                __typename: 'Message',
                                _id: sendMessage._id,
                                from: me._id,
                                activityId: request.activity._id,
                                text: msgText.trim(),
                                createdAt: timeStamp
                              }
                            ],
                            seenBy: [me._id],
                            lastMessageAt: timeStamp,
                            createdAt: timeStamp,
                            updatedAt: timeStamp
                          };

                          next.getChats.unshift(chatObj);
                          cache.writeQuery({ query: GET_CHATS, data: next });
                        }

                        if (index >= 0) {
                          const chatObj = prev.getChats[index];
                          chatObj.messages.unshift({
                            __typename: 'Message',
                            _id: sendMessage._id,
                            from: me._id,
                            activityId: request.activity._id,
                            text: msgText.trim(),
                            createdAt: timeStamp
                          });
                          chatObj.lastMessageAt = timeStamp;
                          chatObj.seenBy = [me._id];
                          next.getChats.splice(index, 1);
                          next.getChats.unshift(chatObj);

                          cache.writeQuery({ query: GET_CHATS, data: next });
                        }
                      }
                    });
                    this.setState({ msgText: null });
                    Keyboard.dismiss();
                    showToast('Nachricht gesendet');
                  }
                }}
              />
            )}
          </Mutation>
        </View>
      </View>
    );
  }
}

export default withNavigation(RequestCompose);
