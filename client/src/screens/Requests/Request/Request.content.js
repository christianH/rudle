import React from 'react';
import { withNavigation } from 'react-navigation';
import {
  Text,
  View,
  StyleSheet,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Image
} from 'react-native';

import {
  colors,
  gutter,
  headerHeight,
  bottomSpace,
  bold
} from '../../../lib/styles';

const progressBorderSize = 8;
const itemSize = 150;
const itemSizeWithBorder = itemSize + progressBorderSize * 3;
const styles = StyleSheet.create({
  cardContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: gutter * 2
  },
  cardTitle: {
    fontSize: 24,
    ...bold,
    textAlign: 'center',
    marginTop: gutter
  },
  cardSubtitle: {
    fontSize: 15,
    textAlign: 'center'
  },
  avatarContainer: {
    height: itemSizeWithBorder,
    width: itemSizeWithBorder,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: itemSizeWithBorder / 2,
    backgroundColor: colors.backgroundColor,
    borderWidth: progressBorderSize,
    borderColor: colors.tintColor,
    marginTop: headerHeight + bottomSpace
  },
  avatarImage: {
    width: itemSize,
    height: itemSize,
    borderRadius: itemSize / 2,
    backgroundColor: colors.secondaryColor
  }
});

class RequestContent extends React.PureComponent {
  render() {
    const { request, navigation } = this.props;
    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={styles.cardContainer}>
          <View style={styles.avatarContainer}>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('User', { userId: request.user._id });
              }}
            >
              <Image
                style={styles.avatarImage}
                source={{ uri: request.user.avatar }}
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Activity', {
                activityId: request.activity._id
              });
            }}
          >
            <Text style={styles.cardTitle}>
              Lust auf {request.activity.title}?
            </Text>
            <Text style={styles.cardSubtitle}>mehr Infos</Text>
          </TouchableOpacity>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

export default withNavigation(RequestContent);
