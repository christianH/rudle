import React from 'react';
import { withNavigation } from 'react-navigation';
import { Query } from 'react-apollo';
import ME from '../../graphql/queries/me';
import GET_REQUESTS from '../../graphql/queries/getRequests';
import Spinner from '../../components/Spinner';
import Error from '../../components/Error';
import Cube3D from '../../components/Cube3D';
import Request from './Request';
import MARK_REQUEST_AS_SEEN from '../../graphql/mutations/markRequestAsSeen';
import Client from '../../apollo';

class Requests extends React.Component {
  shouldComponentUpdate() {
    return false;
  }

  markRequestAsSeen = (request, me) => {
    if (!request.seenBy.includes(me._id)) {
      Client.mutate({
        mutation: MARK_REQUEST_AS_SEEN,
        variables: { requestId: request._id },
        optimisticResponse: {
          __typename: 'Mutation',
          markRequestAsSeen: {
            __typename: 'Status',
            message: 'Request Marked as Seen!'
          }
        },
        update: cache => {
          const prev = cache.readQuery({
            query: GET_REQUESTS,
            variables: { _id: request._id }
          });
          const index = prev.getRequests.findIndex(
            requestObj => requestObj._id === request._id
          );
          const next = prev;
          next.getRequests[index].seenBy = [me._id];
          cache.writeQuery({ query: GET_REQUESTS, data: next });
        }
      });
    }
  };

  render() {
    const { navigation } = this.props;
    const { startIndex } = navigation.state.params;
    return (
      <Query query={ME}>
        {({ data: { me } }) => {
          if (!me) return null;
          return (
            <Query query={GET_REQUESTS}>
              {({ loading, error, data }) => {
                if (loading) return <Spinner />;
                if (error) return <Error />;
                return (
                  <Cube3D
                    startIndex={startIndex}
                    autoplayInterval={5000}
                    onEndReached={() => navigation.goBack()}
                    onScrollCallback={index =>
                      this.markRequestAsSeen(data.getRequests[index], me._id)
                    }
                  >
                    {data.getRequests.map(request => (
                      <Request
                        key={request._id}
                        keyExtractor={request._id}
                        request={request}
                        me={me}
                      />
                    ))}
                  </Cube3D>
                );
              }}
            </Query>
          );
        }}
      </Query>
    );
  }
}

export default withNavigation(Requests);
