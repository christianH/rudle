import React from 'react';
import { ScrollView, View, RefreshControl } from 'react-native';

import { withNavigation } from 'react-navigation';
import { Query } from 'react-apollo';
import Spinner from '../../components/Spinner';
import NoConnection from '../../components/NoConnection';
import ME from '../../graphql/queries/me';
import BackgroundWave from '../../components/BackgroundWave';
import AccountDetails from './Account.details';
import AccountSettings from './Account.settings';
import { colors, content } from '../../lib/styles';

class Account extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false
    };
  }

  onRefresh = () => {
    this.setState({ refreshing: true });
    setTimeout(() => {
      this.setState({ refreshing: false });
    }, 1000);
  };

  render() {
    const { refreshing } = this.state;
    return (
      <Query query={ME}>
        {({ loading, error, data }) => {
          if (loading) return <Spinner />;
          if (error) return <NoConnection />;
          return (
            <ScrollView
              refreshControl={
                <RefreshControl
                  refreshing={refreshing}
                  style={{ zIndex: 1 }}
                  onRefresh={this.onRefresh}
                  progressViewOffset={65}
                  tintColor={colors.shadowColor}
                />
              }
            >
              <View style={{ ...content }}>
                <BackgroundWave color={colors.tintColor} />
                <AccountDetails me={data.me} />
                <AccountSettings settings={data.me.settings} />
              </View>

              {/* TODO: Help & Support...
                  -Help & Support
                  -Community-Guidelines
                  -Sicherheitstipps
                  -Datenschutzerklärung
                  -TermsOfService
                  -Konto löschen
               */}
            </ScrollView>
          );
        }}
      </Query>
    );
  }
}

export default withNavigation(Account);
