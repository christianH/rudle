import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Mutation } from 'react-apollo';
import UPDATE_USER_SETTINGS from '../../graphql/mutations/updateUserSettings';
import ME from '../../graphql/queries/me';
import Slider from '../../components/Slider';
import ListItem from '../../components/ListItem';
import Switch from '../../components/Switch';

import { colors, gutter, bold } from '../../lib/styles';

const styles = StyleSheet.create({
  section: {
    marginTop: 30,
    marginLeft: 20
  },
  sectionHeading: {
    fontSize: 15,
    ...bold,
    marginBottom: gutter / 2
  },
  sliderValue: {
    ...bold,
    color: colors.tintColor,
    alignSelf: 'flex-end',
    marginRight: 20,
    marginTop: -25
  },
  infoText: {
    fontSize: 13,
    lineHeight: 20,
    color: colors.secondaryColor,
    marginLeft: gutter * 2,
    marginRight: gutter * 2
  }
});

class AccountSettings extends React.Component {
  constructor(props) {
    super(props);
    const { settings } = this.props;

    this.state = {
      radius: settings.radius,
      connectWithFavorites: settings.connectWithFavorites,
      connectWithFriends: settings.connectWithFriends,
      connectWithFriendsOfFriends: settings.connectWithFriendsOfFriends,
      connectWithUnknown: settings.connectWithUnknown,
      notificationRequests: settings.notificationRequests,
      notificationMessages: settings.notificationMessages
    };
  }

  render() {
    const { navigation } = this.props;
    const {
      radius,
      connectWithFavorites,
      connectWithFriends,
      connectWithFriendsOfFriends,
      connectWithUnknown,
      notificationRequests,
      notificationMessages
    } = this.state;

    return (
      <Mutation mutation={UPDATE_USER_SETTINGS}>
        {updateUserSettings => (
          <View>
            <View style={styles.section}>
              <Text style={styles.sectionHeading}>
                Wie groß ist dein Revier?
              </Text>
              <Text style={styles.sliderValue}>{radius} km</Text>

              <Slider
                value={radius}
                onValueChange={newRadius =>
                  this.setState({ radius: newRadius })
                }
                onSlidingComplete={newRadius => {
                  updateUserSettings({
                    variables: { radius: newRadius },
                    optimisticResponse: {
                      __typename: 'Mutation',
                      updateUserSettings: {
                        __typename: 'Status',
                        message: 'Settings updated!'
                      }
                    },
                    update: cache => {
                      const prev = cache.readQuery({
                        query: ME
                      });
                      const next = prev;
                      next.me.settings.radius = newRadius;
                      cache.writeQuery({ query: ME, data: next });
                    }
                  });
                }}
              />

              <Text style={styles.infoText}>
                Du erhälst und versendest Anfragen für Aktivitäten nur innerhalb
                deines Reviers.
              </Text>
            </View>

            <View style={styles.section}>
              <Text style={styles.sectionHeading}>
                Wer gehört zu deinem Rudel?
              </Text>
              <ListItem text="Favoriten">
                <Switch
                  value={connectWithFavorites}
                  onValueChange={newConnectWithFavorites => {
                    this.setState({
                      connectWithFavorites: newConnectWithFavorites
                    });
                    updateUserSettings({
                      variables: {
                        connectWithFavorites: newConnectWithFavorites
                      },
                      optimisticResponse: {
                        __typename: 'Mutation',
                        updateUserSettings: {
                          __typename: 'Status',
                          message: 'Settings updated!'
                        }
                      },
                      update: cache => {
                        const prev = cache.readQuery({
                          query: ME
                        });
                        const next = prev;
                        next.me.settings.connectWithFavorites = newConnectWithFavorites;
                        cache.writeQuery({ query: ME, data: next });
                      }
                    });
                  }}
                />
              </ListItem>

              <ListItem text="Freunde">
                <Switch
                  value={connectWithFriends}
                  onValueChange={newConnectWithFriends => {
                    this.setState({
                      connectWithFriends: newConnectWithFriends
                    });
                    updateUserSettings({
                      variables: {
                        connectWithFriends: newConnectWithFriends
                      },
                      optimisticResponse: {
                        __typename: 'Mutation',
                        updateUserSettings: {
                          __typename: 'Status',
                          message: 'Settings updated!'
                        }
                      },
                      update: cache => {
                        const prev = cache.readQuery({
                          query: ME
                        });
                        const next = prev;
                        next.me.settings.connectWithFriends = newConnectWithFriends;
                        cache.writeQuery({ query: ME, data: next });
                      }
                    });
                  }}
                />
              </ListItem>

              <ListItem text="Freunde von Freunden">
                <Switch
                  value={connectWithFriendsOfFriends}
                  onValueChange={newConnectWithFriendsOfFriends => {
                    this.setState({
                      connectWithFriendsOfFriends: newConnectWithFriendsOfFriends
                    });
                    updateUserSettings({
                      variables: {
                        connectWithFriendsOfFriends: newConnectWithFriendsOfFriends
                      },
                      optimisticResponse: {
                        __typename: 'Mutation',
                        updateUserSettings: {
                          __typename: 'Status',
                          message: 'Settings updated!'
                        }
                      },
                      update: cache => {
                        const prev = cache.readQuery({
                          query: ME
                        });
                        const next = prev;
                        next.me.settings.connectWithFriendsOfFriends = newConnectWithFriendsOfFriends;
                        cache.writeQuery({ query: ME, data: next });
                      }
                    });
                  }}
                />
              </ListItem>

              <ListItem text="Unbekannte">
                <Switch
                  value={connectWithUnknown}
                  onValueChange={newConnectWithUnknown => {
                    this.setState({
                      connectWithUnknown: newConnectWithUnknown
                    });
                    updateUserSettings({
                      variables: {
                        connectWithUnknown: newConnectWithUnknown
                      },
                      optimisticResponse: {
                        __typename: 'Mutation',
                        updateUserSettings: {
                          __typename: 'Status',
                          message: 'Settings updated!'
                        }
                      },
                      update: cache => {
                        const prev = cache.readQuery({
                          query: ME
                        });
                        const next = prev;
                        next.me.settings.connectWithUnknown = newConnectWithUnknown;
                        cache.writeQuery({ query: ME, data: next });
                      }
                    });
                  }}
                />
              </ListItem>
              <ListItem
                text="Rudel verwalten"
                onPress={() => navigation.navigate('Activities')} // TODO:
              />

              <ListItem
                text="Freunde zu Rudle einladen"
                onPress={() => navigation.navigate('Activities')}
              />
            </View>

            <View style={styles.section}>
              <Text style={styles.sectionHeading}>Benachrichtigungen</Text>
              <ListItem text="Neue Anfragen">
                <Switch
                  value={notificationRequests}
                  onValueChange={newNotificationRequests => {
                    this.setState({
                      notificationRequests: newNotificationRequests
                    });
                    updateUserSettings({
                      variables: {
                        notificationRequests: newNotificationRequests
                      },
                      optimisticResponse: {
                        __typename: 'Mutation',
                        updateUserSettings: {
                          __typename: 'Status',
                          message: 'Settings updated!'
                        }
                      },
                      update: cache => {
                        const prev = cache.readQuery({
                          query: ME
                        });
                        const next = prev;
                        next.me.settings.notificationRequests = newNotificationRequests;
                        cache.writeQuery({ query: ME, data: next });
                      }
                    });
                  }}
                />
              </ListItem>

              <ListItem text="Nachrichten">
                <Switch
                  value={notificationMessages}
                  onValueChange={newNotificationMessages => {
                    this.setState({
                      notificationMessages: newNotificationMessages
                    });
                    updateUserSettings({
                      variables: {
                        notificationMessages: newNotificationMessages
                      },
                      optimisticResponse: {
                        __typename: 'Mutation',
                        updateUserSettings: {
                          __typename: 'Status',
                          message: 'Settings updated!'
                        }
                      },
                      update: cache => {
                        const prev = cache.readQuery({
                          query: ME
                        });
                        const next = prev;
                        next.me.settings.notificationMessages = newNotificationMessages;
                        cache.writeQuery({ query: ME, data: next });
                      }
                    });
                  }}
                />
              </ListItem>
            </View>
          </View>
        )}
      </Mutation>
    );
  }
}

export default withNavigation(AccountSettings);
