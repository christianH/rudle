import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Avatar } from 'react-native-elements';
import UserLocation from '../User/User.location';
import calculateAge from '../User/User.calculateAge';
import { gutter, shadow, bold } from '../../lib/styles';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginVertical: gutter,
    marginHorizontal: 20
  },
  userName: {
    fontSize: 24,
    ...bold,
    marginTop: gutter,
    marginHorizontal: 20
  }
});

class AccountDetails extends React.PureComponent {
  render() {
    const { navigation, me } = this.props;
    return (
      <View style={styles.container}>
        <Avatar
          size="xlarge"
          rounded
          source={{ uri: me.avatar }}
          containerStyle={{ ...shadow }}
          onPress={() => navigation.navigate('User', { userId: me._id })}
        />
        <Text style={styles.userName}>
          {me.name}
          {me.birthday ? `, ${calculateAge(me.birthday)}` : null}
        </Text>
        <UserLocation location={me.location} />
      </View>
    );
  }
}

export default withNavigation(AccountDetails);
