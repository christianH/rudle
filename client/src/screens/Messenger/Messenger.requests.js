import React from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import { Avatar } from 'react-native-elements';
import { withNavigation } from 'react-navigation';
import Icon from '@expo/vector-icons/MaterialIcons';
import { Query } from 'react-apollo';
import ME from '../../graphql/queries/me';
// import GET_MY_LAST_REQUEST from '../../graphql/queries/getMyLastRequest';
// import Spinner from '../../components/Spinner';
// import Error from '../../components/Error';
import { colors, gutter, shadow } from '../../lib/styles';

const itemSize = 60;
const itemSpacing = 6;
const itemBorder = 5;
const itemShadow = 4;
const itemContainerSize = itemSize + itemBorder * 2;

const styles = StyleSheet.create({
  contentContainer: {
    height: itemContainerSize + itemShadow,
    paddingLeft: gutter - itemBorder,
    paddingRight: gutter - itemBorder - itemSpacing
  },
  itemContainer: {
    height: itemContainerSize,
    width: itemContainerSize,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: itemSpacing,
    borderRadius: itemContainerSize / 2,
    backgroundColor: colors.backgroundColor
  },
  unseen: {
    borderColor: colors.tintColor,
    borderWidth: itemBorder - 2,
    ...shadow
  },
  seen: {
    borderColor: colors.secondaryColor,
    borderWidth: 1
  },
  addNewContainer: {
    height: itemSize,
    width: itemSize,
    borderRadius: itemSize / 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.secondaryColor,
    borderColor: colors.backgroundColor,
    borderWidth: 0,
    margin: itemBorder,
    marginRight: itemBorder * 2,
    ...shadow
  },
  iconContainer: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    height: 22,
    width: 22,
    borderRadius: 11,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1.4,
    borderColor: colors.backgroundColor,
    backgroundColor: colors.secondaryColor
  }
});

class MessengerRequests extends React.PureComponent {
  componentDidMount() {
    const { subscribeToRequests } = this.props;
    subscribeToRequests();
  }

  // renderMyRequest = () => {
  //   const { navigation } = this.props;
  //   return (
  //     <Query query={GET_MY_LAST_REQUEST}>
  //       {({ loading, error, data }) => {
  //         if (loading || error || !data.getMyLastRequest)
  //           return this.renderAddNewRequest();

  //         return (
  //           <View style={[styles.itemContainer]}>
  //             <Avatar
  //               size={60}
  //               rounded
  //               source={{
  //                 uri: data.getMyLastRequest.user.avatar
  //               }}
  //               onPress={() =>
  //                 navigation.navigate('Requests', { startIndex: 0 })
  //               }
  //             />
  //             <View style={styles.iconContainer}>
  //               <Icon size={18} name="add" color="#fff" />
  //             </View>
  //           </View>
  //         );
  //       }}
  //     </Query>
  //   );
  // };

  renderAddNewRequest = () => {
    const { navigation } = this.props;
    return (
      <View style={styles.addNewContainer}>
        <Avatar
          size={itemSize}
          rounded
          onPress={() => navigation.navigate('Activities')}
        />
        <View style={styles.iconContainer}>
          <Icon size={18} name="add" color="#fff" />
        </View>
      </View>
    );
  };

  renderListItem = ({ item, index }) => {
    const { navigation } = this.props;
    return (
      <Query query={ME}>
        {({ data: { me } }) => {
          if (!me) return null;
          const isSeen = item.seenBy.includes(me._id);
          return (
            <View
              style={[
                styles.itemContainer,
                isSeen ? styles.seen : styles.unseen
              ]}
              key={item._id}
            >
              <Avatar
                size={60}
                rounded
                source={{ uri: item.user.avatar }}
                onPress={() =>
                  navigation.navigate('Requests', { startIndex: index })
                }
              />
            </View>
          );
        }}
      </Query>
    );
  };

  render() {
    const { requests } = this.props;

    return (
      <FlatList
        horizontal
        data={requests}
        keyExtractor={item => item._id}
        renderItem={this.renderListItem}
        // ListHeaderComponent={this.renderAddNewRequest}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={styles.contentContainer}
      />
    );
  }
}

export default withNavigation(MessengerRequests);
