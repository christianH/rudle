/* eslint-disable no-shadow */
import React from 'react';
import dayjs from 'dayjs';

import { View, Text, FlatList, StyleSheet, RefreshControl } from 'react-native';
import { ListItem, Avatar } from 'react-native-elements';
import { withNavigation } from 'react-navigation';
import { Query } from 'react-apollo';
import Spinner from '../../components/Spinner';
import NoConnection from '../../components/NoConnection';
import ME from '../../graphql/queries/me';
import GET_AVATAR from '../../graphql/queries/getAvatar';
import GET_CHATS from '../../graphql/queries/getChats';
import MESSAGE_ADDED from '../../graphql/subscriptions/messageAdded';
import MessengerHeader from './Messenger.header';
import ItemSeparator from '../../components/ItemSeparator';
import { colors, gutter, shadow, bold, content } from '../../lib/styles';

const itemSize = 60;
const styles = StyleSheet.create({
  listItemContainer: {
    borderBottomWidth: 0,
    paddingHorizontal: gutter,
    paddingVertical: gutter / 2
  },
  isNew: {
    backgroundColor: colors.tintColorLight2
  },
  listItemTitle: {
    fontSize: 15,
    lineHeight: 20,
    ...bold
  },
  listItemSubTitle: {
    fontSize: 15,
    lineHeight: 20,
    color: colors.textDekorColor
  },
  badge: {
    width: 12,
    height: 12,
    marginLeft: 12,
    marginRight: 8,
    marginTop: 4,
    borderRadius: 10,
    backgroundColor: colors.tintColor
  },
  timestamp: {
    fontSize: 13,
    lineHeight: 15
  }
});

class Messenger extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false
    };
  }

  onRefresh = () => {
    this.setState({ refreshing: true });
    setTimeout(() => {
      this.setState({ refreshing: false });
    }, 1000);
  };

  renderListHeader = (subscribeToChats, hasChats) => (
    <MessengerHeader subscribeToChats={subscribeToChats} hasChats={hasChats} />
  );

  renderItemSeparator = () => <ItemSeparator itemSize={itemSize} />;

  renderListFooter = hasChats =>
    hasChats ? <ItemSeparator itemSize={itemSize} /> : null;

  renderAvatar = avatar => (
    <Avatar
      size={itemSize}
      rounded
      source={{ uri: avatar }}
      containerStyle={{ ...shadow }}
    />
  );

  renderRightIcon = (item, isSeen) => (
    <View>
      <Text
        style={[
          styles.timestamp,
          { color: !isSeen ? colors.tintColor : colors.secondaryColor }
        ]}
      >
        {dayjs().isSameOrBefore(item.lastMessageAt, 'day')
          ? dayjs(item.lastMessageAt).format('HH:mm')
          : dayjs().isSameOrBefore(item.lastMessageAt, 'week')
          ? dayjs(item.lastMessageAt).format('dddd')
          : dayjs(item.lastMessageAt).format('DD.MM.YY')}
      </Text>
      {!isSeen ? <View style={styles.badge} /> : null}
    </View>
  );

  renderListItem = ({ item }) => {
    const { navigation } = this.props;
    return (
      <Query query={ME}>
        {({ loading, error, data: { me } }) => {
          if (loading) return <Spinner />;
          if (error) return null;
          const userId = item.members.find(_id => _id !== me._id);
          const isSeen = item.seenBy.includes(me._id);

          return (
            <Query query={GET_AVATAR} variables={{ _id: userId }}>
              {({ loading, error, data: { getUser } }) => {
                if (loading) return <Spinner />;
                if (error) return null;
                return (
                  <ListItem
                    key={item._id}
                    title={getUser.name}
                    subtitle={item.messages[0].text}
                    subtitleProps={{ numberOfLines: 1 }}
                    leftAvatar={this.renderAvatar(getUser.avatar)}
                    titleStyle={styles.listItemTitle}
                    subtitleStyle={styles.listItemSubTitle}
                    rightIcon={this.renderRightIcon(item, isSeen)}
                    containerStyle={[
                      styles.listItemContainer,
                      isSeen ? null : styles.isNew
                    ]}
                    onPress={() => {
                      navigation.navigate('Chat', {
                        chatId: item._id,
                        userId
                      });
                    }}
                  />
                );
              }}
            </Query>
          );
        }}
      </Query>
    );
  };

  render() {
    const { refreshing } = this.state;
    return (
      <Query query={GET_CHATS}>
        {({ loading, error, data, subscribeToMore }) => {
          if (loading) return <Spinner />;
          if (error) return <NoConnection />;
          const hasChats = data.getChats.length > 0;
          const subscribeToChats = () =>
            subscribeToMore({
              document: MESSAGE_ADDED,
              updateQuery: (prev, { subscriptionData }) => {
                if (!subscriptionData.data) return prev;
                const next = prev;
                const index = prev.getChats.findIndex(
                  chat => chat._id === subscriptionData.data.messageAdded._id
                );
                if (index >= 0) next.getChats.splice(index, 1);
                next.getChats.unshift(subscriptionData.data.messageAdded);
                return next;
              }
            });

          return (
            <FlatList
              data={data.getChats}
              keyExtractor={item => item._id}
              renderItem={this.renderListItem}
              ItemSeparatorComponent={this.renderItemSeparator}
              contentContainerStyle={{ ...content }}
              ListHeaderComponent={this.renderListHeader(
                subscribeToChats,
                hasChats
              )}
              ListFooterComponent={this.renderListFooter(hasChats)}
              refreshControl={
                <RefreshControl
                  refreshing={refreshing}
                  style={{ zIndex: 1 }}
                  onRefresh={this.onRefresh}
                  progressViewOffset={65}
                  tintColor={colors.shadowColor}
                />
              }
            />
          );
        }}
      </Query>
    );
  }
}

export default withNavigation(Messenger);
