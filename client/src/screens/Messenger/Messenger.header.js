import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Query } from 'react-apollo';
import MessengerRequests from './Messenger.requests';
import GET_REQUESTS from '../../graphql/queries/getRequests';
import REQUESTS_ADDED from '../../graphql/subscriptions/requestAdded';
import Spinner from '../../components/Spinner';
import Error from '../../components/Error';
import BackgroundWave from '../../components/BackgroundWave';
import SearchBox from '../../components/Searchbox';
import ItemSeparator from '../../components/ItemSeparator';

import { gutter, colors, bold } from '../../lib/styles';

const itemSize = 60;

const styles = StyleSheet.create({
  listSection: {
    marginTop: 30 - 4,
    marginHorizontal: gutter
  },
  listHeading: {
    fontSize: 15,
    ...bold,
    marginBottom: gutter / 2
  },
  carouselSection: {
    marginTop: gutter * 2.7
  },
  carouselHeading: {
    fontSize: 15,
    ...bold,
    marginBottom: gutter / 2,
    marginLeft: gutter
  }
});

class MessengerHeader extends React.PureComponent {
  componentDidMount() {
    const { subscribeToChats } = this.props;
    subscribeToChats();
  }

  render() {
    const { hasChats } = this.props;
    return (
      <View>
        <BackgroundWave color={colors.tintColor} />

        <Query query={GET_REQUESTS}>
          {({ loading, error, data, subscribeToMore }) => {
            if (loading) return <Spinner />;
            if (error) return <Error />;

            const subscribeToRequests = () =>
              subscribeToMore({
                document: REQUESTS_ADDED,
                updateQuery: (prev, { subscriptionData }) => {
                  if (!subscriptionData.data) return prev;
                  const newRequest = subscriptionData.data.requestAdded;
                  if (prev.getRequests.length > 20) prev.getRequests.pop(); // remove last request
                  return Object.assign({}, prev, {
                    getRequests: [newRequest, ...prev.getRequests]
                  });
                }
              });

            return (
              <View>
                <SearchBox placeholder="Messenger durchsuchen" />

                <View style={styles.carouselSection}>
                  <Text style={styles.carouselHeading}>Anfragen</Text>
                  <MessengerRequests
                    requests={data.getRequests}
                    subscribeToRequests={subscribeToRequests}
                  />
                </View>

                <View style={styles.listSection}>
                  <Text style={styles.listHeading}>Nachrichten</Text>
                </View>
                {hasChats ? <ItemSeparator itemSize={itemSize} /> : null}
              </View>
            );
          }}
        </Query>
      </View>
    );
  }
}

export default MessengerHeader;
