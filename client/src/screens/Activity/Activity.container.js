import React from 'react';
import { View } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Query } from 'react-apollo';
import GET_ACTIVITY from '../../graphql/queries/getActivity';
import Spinner from '../../components/Spinner';
import Error from '../../components/Error';
import PopUp from '../../components/PopUp';
import ImageSlider from '../../components/ImageSlider';
import ActivityInfo from './Activity.info';
import ActivityRecommendations from './Activity.recommendations';
import { windowWidth, windowHeight } from '../../lib/styles';

class Activity extends React.PureComponent {
  render() {
    const { navigation } = this.props;
    return (
      <PopUp statusBarSwitchToSolid={windowHeight * 0.33}>
        <Query
          query={GET_ACTIVITY}
          variables={{ _id: navigation.state.params.activityId }}
        >
          {({ loading, error, data }) => {
            if (loading) return <Spinner />;
            if (error) return <Error />;
            return (
              <View>
                <ImageSlider
                  images={[data.getActivity.image]}
                  width={windowWidth}
                  height={windowHeight * 0.33}
                />
                <ActivityInfo activity={data.getActivity} />
                <ActivityRecommendations activity={data.getActivity} />
              </View>
            );
          }}
        </Query>
      </PopUp>
    );
  }
}

export default withNavigation(Activity);
