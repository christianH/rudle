import React from 'react';
import { withNavigation } from 'react-navigation';
import { Text, View, StyleSheet } from 'react-native';
import ActivityButton from './Activity.button';
import { flex, colors, bold } from '../../lib/styles';

const styles = StyleSheet.create({
  section: {
    marginTop: 30,
    marginHorizontal: 20
  },
  cardTitle: {
    fontSize: 24,
    ...bold
  },
  cardInfoText: {
    fontSize: 15,
    color: colors.secondaryColor
  },
  cardText: {
    fontSize: 15,
    lineHeight: 20,
    color: colors.textDekorColor,
    marginTop: 20
  }
});

class ActivityInfo extends React.PureComponent {
  render() {
    const { activity } = this.props;
    return (
      <View style={styles.section}>
        <View style={flex.row}>
          <View style={flex.left}>
            <Text style={styles.cardTitle}>{activity.title}</Text>
            <Text style={styles.cardInfoText}>
              {activity.totalRequests} mal gerudelt
            </Text>
          </View>
          <View style={flex.right}>
            <ActivityButton activity={activity} />
          </View>
        </View>
        <Text style={styles.cardText}>{activity.description}</Text>
      </View>
    );
  }
}

export default withNavigation(ActivityInfo);
