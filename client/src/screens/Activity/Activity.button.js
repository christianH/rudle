import React from 'react';
import { StyleSheet, TouchableNativeFeedback } from 'react-native';
import { Button } from 'react-native-elements';
import { Mutation } from 'react-apollo';
import SEND_REQUEST from '../../graphql/mutations/sendRequest';
import GET_ACTIVITY from '../../graphql/queries/getActivity';
import ME from '../../graphql/queries/me';
// import GET_MY_LAST_REQUEST from '../../graphql/queries/getMyLastRequest';
import showToast from '../../components/Toast';
import { colors, gutter, shadow, bold } from '../../lib/styles';

const styles = StyleSheet.create({
  button: {
    minWidth: 100,
    height: 40,
    backgroundColor: colors.tintColor,
    paddingHorizontal: 10,
    borderRadius: 20,
    marginTop: -5,
    ...shadow
  },
  buttonTitle: {
    fontSize: 15,
    ...bold
  },
  buttonDisabled: {
    backgroundColor: colors.secondaryColor,
    borderRadius: gutter * 2,
    paddingHorizontal: 10
  },
  buttonTitleDisabled: {
    fontSize: 15,
    ...bold
  }
});

class ActivityButton extends React.PureComponent {
  state = {
    disabled: false
  };

  render() {
    const { disabled } = this.state;
    const { activity } = this.props;
    return (
      <Mutation mutation={SEND_REQUEST}>
        {sendRequest => (
          <Button
            title="RUDELN"
            titleStyle={styles.buttonTitle}
            buttonStyle={styles.button}
            background={TouchableNativeFeedback.SelectableBackground()} // Hack to use borderRadius
            disabledStyle={styles.buttonDisabled}
            disabledTitleStyle={styles.buttonTitleDisabled}
            disabled={!!disabled}
            onPress={() => {
              this.setState({ disabled: !disabled });
              sendRequest({
                variables: { activityId: activity._id },
                refetchQueries: [
                  { query: GET_ACTIVITY, variables: { _id: activity._id } },
                  { query: ME }
                  // { query: GET_MY_LAST_REQUEST }
                ]
              });
              showToast('Anfrage gesendet');
            }}
          />
        )}
      </Mutation>
    );
  }
}

export default ActivityButton;
