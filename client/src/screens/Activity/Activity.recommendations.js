import React from 'react';
import { Text, View, FlatList, Image, StyleSheet, Linking } from 'react-native';
import { withNavigation } from 'react-navigation';
import { ListItem } from 'react-native-elements';
import ItemSeparator from '../../components/ItemSeparator';
import Spinner from '../../components/Spinner';
import { colors, gutter, bold } from '../../lib/styles';

const itemSize = 60;

const styles = StyleSheet.create({
  section: {
    marginTop: 30,
    marginHorizontal: 20
  },
  listHeading: {
    fontSize: 15,
    ...bold,
    marginBottom: gutter / 4
  },
  listItemContainer: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingVertical: 10
  },
  listItemTitle: {
    fontSize: 15,
    lineHeight: 20,
    ...bold
  },
  listItemSubTitle: {
    fontSize: 15,
    color: colors.textDekorColor
  },
  listItemImage: {
    height: itemSize,
    width: itemSize
  }
});

// TODO: GET_RECOMMENDATIONS
const recommendations = [
  {
    id: '1',
    title: 'Netflix',
    description:
      'Lorem ipsum dolor sit amet, consetetur elitr, sed diam nonumy eirmod tempor.',
    link: 'http://www.netflix.com',
    imageURI: 'https://art.pixilart.com/705ba833f935409.png'
  },
  {
    id: '2',
    title: 'TripAdvisor',
    description:
      'Lorem ipsum dolor sit amet, consetetur elitr, sed diam nonumy eirmod tempor.',
    link: 'https://www.tripadvisor.de',
    imageURI:
      'https://i2.wp.com/sssicamous.ca/wp-content/uploads/2017/10/TripAdvisor-logo-3.jpg'
  },
  {
    id: '3',
    title: 'Jochen Schweitzer',
    description:
      'Lorem ipsum dolor sit amet, consetetur elitr, sed diam nonumy eirmod tempor.',
    link: 'http://www.jochen-schweitzer.com',
    imageURI:
      'https://webimg.secondhandapp.com/Jochen-Schweizer-Gutschein-16ae1832.jpg'
  },

  {
    id: '4',
    title: 'Funny-Frisch',
    description:
      'Lorem ipsum dolor sit amet, consetetur elitr, sed diam nonumy eirmod tempor.',
    link: 'http://www.funny-frisch.com',
    imageURI: 'https://upload.wikimedia.org/wikipedia/de/a/a4/Funny_Frisch.jpg'
  },
  {
    id: '5',
    title: 'CenterParcs',
    description:
      'Lorem ipsum dolor sit amet, consetetur elitr, sed diam nonumy eirmod tempor.',
    link: 'https://www.centerparcs.de',
    imageURI: 'https://static1.centerparcs.com/95.2/img/logo-cp.png'
  },
  {
    id: '6',
    title: 'Edeka',
    description:
      'Lorem ipsum dolor sit amet, consetetur elitr, sed diam nonumy eirmod tempor.',
    link: 'http://www.edeka.de',
    imageURI:
      'http://spvgaurich.de/typo3template/user_upload/edeka-Logo-Vector-Download.jpg'
  }
];

class ActivityRecommendations extends React.PureComponent {
  renderListHeader = () => {
    const { activity } = this.props;
    return (
      <View style={styles.section}>
        <Text style={styles.listHeading}>Passt zu {activity.title}:</Text>
      </View>
    );
  };

  renderListItem = ({ item }) => (
    <ListItem
      key={item._id}
      title={item.title}
      titleStyle={styles.listItemTitle}
      subtitle={item.description}
      subtitleProps={{ numberOfLines: 2 }}
      subtitleStyle={styles.listItemSubTitle}
      leftElement={
        <Image style={styles.listItemImage} source={{ uri: item.imageURI }} />
      }
      chevron
      chevronColor={colors.secondaryColor}
      containerStyle={styles.listItemContainer}
      onPress={() => {
        Linking.openURL(item.link);
      }}
    />
  );

  renderItemSeparator = () => (
    <ItemSeparator itemSize={itemSize + gutter / 2} />
  );

  renderSpinner = () => (
    <View>
      <ItemSeparator itemSize={itemSize + gutter / 2} />
      <Spinner />
    </View>
  );

  render() {
    return (
      <FlatList
        data={recommendations}
        keyExtractor={item => item.id}
        renderItem={this.renderListItem}
        ItemSeparatorComponent={this.renderItemSeparator}
        ListHeaderComponent={this.renderListHeader}
        ListFooterComponent={this.renderSpinner}
        // onEndReached={() => loadMore()}
        // onEndReachedThreshold={0.5}
        // initialNumToRender={8}
        // maxToRenderPerBatch={2}
      />
    );
  }
}

export default withNavigation(ActivityRecommendations);
