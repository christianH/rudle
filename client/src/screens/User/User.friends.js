import React from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Avatar } from 'react-native-elements';
import { graphql, Query } from 'react-apollo';
import Spinner from '../../components/Spinner';
import Error from '../../components/Error';
import GET_AVATAR from '../../graphql/queries/getAvatar';
import ME from '../../graphql/queries/me';
import { gutter, shadow, bold } from '../../lib/styles';

const itemSize = 60;
const shadowWidth = 4;
const styles = StyleSheet.create({
  section: {
    marginTop: 30,
    marginHorizontal: 20
  },
  carouselHeading: {
    fontSize: 15,
    ...bold,
    marginBottom: gutter / 2
  },
  carouselContentContainer: {
    height: itemSize + shadowWidth,
    paddingHorizontal: 20
  },
  carouselItem: {
    marginRight: gutter / 2,
    ...shadow
  }
});

class UserFriends extends React.PureComponent {
  renderListFooter = () => <Spinner />;

  renderListItem = ({ item }) => {
    const { navigation } = this.props;
    return (
      <Query query={GET_AVATAR} variables={{ _id: item }}>
        {({ loading, error, data }) => {
          if (loading) return <Spinner />;
          if (error) return <Error />;
          return (
            <Avatar
              size={itemSize}
              rounded
              source={{ uri: data.getUser.avatar }}
              containerStyle={styles.carouselItem}
              onPress={() => {
                navigation.replace('User', { userId: data.getUser._id });
              }}
            />
          );
        }}
      </Query>
    );
  };

  render() {
    const { data, user } = this.props;
    const myFriends = new Set(data.me.friends);
    const userFriends = new Set(user.friends);
    const commonFriends = Array.from(
      new Set([...myFriends].filter(friend => userFriends.has(friend)))
    );

    if (commonFriends.length === 0) return null;

    return (
      <View>
        <View style={styles.section}>
          <Text style={styles.carouselHeading}>
            {commonFriends.length}
            {data.me._id !== user._id ? ' gemeinsame Freunde:' : ' Freunde:'}
          </Text>
        </View>
        <FlatList
          horizontal
          data={commonFriends}
          keyExtractor={item => item}
          renderItem={this.renderListItem}
          ListFooterComponent={this.renderListFooter}
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={styles.carouselContentContainer}
          initialNumToRender={12}
          // onEndReached={() => loadMore()}
          // onEndReachedThreshold={0.5}
        />
      </View>
    );
  }
}

export default withNavigation(graphql(ME)(UserFriends));
