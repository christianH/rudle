import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Button } from 'react-native-elements';
import { gutter, colors, bold } from '../../lib/styles';

const styles = StyleSheet.create({
  section: {
    marginTop: 30,
    paddingHorizontal: 20
  },
  listHeading: {
    fontSize: 15,
    ...bold,
    marginBottom: gutter / 2
  },
  gridList: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  button: {
    borderRadius: gutter,
    backgroundColor: 'transparent',
    borderColor: colors.tintColor,
    borderWidth: 2,
    paddingHorizontal: gutter,
    marginRight: gutter / 4,
    marginBottom: gutter / 2
  },
  buttonTitle: {
    fontSize: 15,
    ...bold,
    color: colors.tintColor
  }
});

class UserInterests extends React.PureComponent {
  render() {
    const { user, navigation } = this.props;
    if (user.interests.length === 0) return null;

    const interests = user.interests
      .sort((obj1, obj2) => obj2.count - obj1.count)
      .slice(0, 9);
    return (
      <View style={styles.section}>
        <Text style={styles.listHeading}>Interessen:</Text>
        <View style={styles.gridList}>
          {interests.map(interest => (
            <Button
              key={interest.activity._id}
              title={interest.activity.title}
              clear
              titleStyle={styles.buttonTitle}
              buttonStyle={styles.button}
              onPress={() =>
                navigation.navigate('Activity', {
                  activityId: interest.activity._id
                })
              }
            />
          ))}
        </View>
      </View>
    );
  }
}

export default withNavigation(UserInterests);
