import React from 'react';
import { StyleSheet } from 'react-native';
import Icon from '@expo/vector-icons/MaterialIcons';
import { Mutation, graphql } from 'react-apollo';
import ADD_USER_TO_FAVORITES from '../../graphql/mutations/addUserToFavorites';
import REMOVE_USER_FROM_FAVORITES from '../../graphql/mutations/removeUserFromFavorites';
import ME from '../../graphql/queries/me';
import showToast from '../../components/Toast';
import { colors } from '../../lib/styles';

const styles = StyleSheet.create({
  favoriteIcon: {
    marginTop: -5
  }
});

class UserFavoriteIcon extends React.PureComponent {
  constructor(props) {
    super(props);
    const { data, user } = this.props;
    this.state = {
      isFavorite: data.me.favorites.includes(user._id)
    };
  }

  render() {
    const { user, data } = this.props;
    const { isFavorite } = this.state;

    if (data.me._id === user._id) return null;

    if (isFavorite) {
      return (
        <Mutation mutation={REMOVE_USER_FROM_FAVORITES}>
          {removeUserFromFavorites => (
            <Icon
              name="favorite-border"
              size={33}
              color={colors.tintColor}
              style={styles.favoriteIcon}
              onPress={() => {
                this.setState({ isFavorite: false });
                removeUserFromFavorites({
                  variables: { userId: user._id },
                  refetchQueries: [{ query: ME }]
                });
                showToast('aus Favoriten entfernt');
              }}
            />
          )}
        </Mutation>
      );
    }
    return (
      <Mutation mutation={ADD_USER_TO_FAVORITES}>
        {addUserToFavorites => (
          <Icon
            name="favorite-border"
            size={33}
            color={colors.secondaryColor}
            style={styles.favoriteIcon}
            onPress={() => {
              this.setState({ isFavorite: true });
              addUserToFavorites({
                variables: { userId: user._id },
                refetchQueries: [{ query: ME }]
              });
              showToast('zu Favoriten hinzugefügt');
            }}
          />
        )}
      </Mutation>
    );
  }
}

export default graphql(ME)(UserFavoriteIcon);
