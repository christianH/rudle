import React from 'react';
import { View } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Query } from 'react-apollo';
import GET_USER from '../../graphql/queries/getUser';
import Spinner from '../../components/Spinner';
import Error from '../../components/Error';
import PopUp from '../../components/PopUp';
import ImageSlider from '../../components/ImageSlider';

import UserInfo from './User.info';
import UserFriends from './User.friends';
import UserInterests from './User.interests';
import UserActionButton from './User.actionButton';

import { windowWidth, windowHeight } from '../../lib/styles';

const sliderHeight = windowHeight * 0.66;

class User extends React.PureComponent {
  render() {
    const { navigation } = this.props;
    return (
      <Query
        query={GET_USER}
        variables={{ _id: navigation.state.params.userId }}
        fetchPolicy="network-only"
      >
        {({ loading, error, data }) => {
          if (loading) return <Spinner />;
          if (error) return <Error />;
          return (
            <View>
              <PopUp statusBarSwitchToSolid={sliderHeight}>
                <ImageSlider
                  images={data.getUser.pictures}
                  width={windowWidth}
                  height={sliderHeight}
                />
                <UserInfo user={data.getUser} />
                <UserInterests user={data.getUser} />
                <UserFriends user={data.getUser} />
                <UserActionButton user={data.getUser} />
              </PopUp>
            </View>
          );
        }}
      </Query>
    );
  }
}

export default withNavigation(User);
