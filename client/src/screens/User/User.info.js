import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import UserLocation from './User.location';
import UserFavoriteIcon from './User.favoriteIcon';
import calculateAge from './User.calculateAge';
import { flex, colors, bold } from '../../lib/styles';

const styles = StyleSheet.create({
  cardHeader: {
    marginTop: 30,
    marginHorizontal: 20
  },
  cardTitle: {
    fontSize: 24,
    ...bold
  },
  cardInfoText: {
    fontSize: 15,
    color: colors.secondaryColor
  },
  cardText: {
    fontSize: 15,
    lineHeight: 20,
    color: colors.textDekorColor,
    marginTop: 20
  }
});

class UserInfo extends React.PureComponent {
  render() {
    const { user } = this.props;
    return (
      <View style={styles.cardHeader}>
        <View style={flex.row}>
          <View style={flex.left}>
            <Text style={styles.cardTitle}>
              {user.name}
              {user.birthday ? `, ${calculateAge(user.birthday)}` : null}
            </Text>
            <UserLocation location={user.location} />
          </View>

          <View style={flex.right}>
            <UserFavoriteIcon user={user} />
          </View>
        </View>

        {user.about ? <Text style={styles.cardText}>{user.about}</Text> : null}
      </View>
    );
  }
}

export default UserInfo;
