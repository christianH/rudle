const calculateAge = birthday => {
  const dob = new Date(birthday);
  const diffMS = Date.now() - dob.getTime();
  const ageDT = new Date(diffMS);
  return Math.abs(ageDT.getUTCFullYear() - 1970);
};

export default calculateAge;
