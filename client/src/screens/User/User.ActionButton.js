import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import { gutter, colors, bold } from '../../lib/styles';

const styles = StyleSheet.create({
  section: {
    marginTop: 30,
    paddingHorizontal: 20
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: StyleSheet.hairlineWidth,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: colors.secondaryColor,
    paddingVertical: gutter * 1.5
  },
  buttonText: {
    fontSize: 15,
    ...bold,
    color: colors.secondaryColor
  }
});

class UserActionButton extends React.PureComponent {
  render() {
    const { user } = this.props;
    return (
      <View style={styles.section}>
        <Button
          title={`${user.name.toUpperCase()} MELDEN`}
          clear
          titleStyle={styles.buttonText}
          buttonStyle={{
            backgroundColor: 'transparent'
          }}
          containerStyle={styles.buttonContainer}
        />
      </View>
    );
  }
}

export default UserActionButton;
