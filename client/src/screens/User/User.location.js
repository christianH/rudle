import React from 'react';
import { View, Text } from 'react-native';
import Icon from '@expo/vector-icons/MaterialIcons';
import { Location } from 'expo';

import { colors, flex } from '../../lib/styles';

class UserLocation extends React.PureComponent {
  constructor(props) {
    super(props);
    this.isComponentMounted = false;
    this.state = {
      locationText: 'Standort laden...'
    };
  }

  componentDidMount() {
    this.isComponentMounted = true;
    this.getUserLocationText();
  }

  componentWillUnmount() {
    this.isComponentMounted = false;
  }

  async getUserLocationText() {
    const { location } = this.props;
    try {
      const geo = await Location.reverseGeocodeAsync({
        latitude: location.latitude,
        longitude: location.longitude
      });
      if (this.isComponentMounted) {
        this.setState({ locationText: geo[0].city });
      }
    } catch (error) {
      if (this.isComponentMounted) {
        this.setState({ locationText: 'Standort nicht verfügbar' });
      }
    }
  }

  render() {
    const { locationText } = this.state;
    return (
      <View style={[flex.row, { alignItems: 'center', marginTop: 1 }]}>
        <Icon
          name="location-on"
          style={{
            fontSize: 15,
            marginLeft: -1,
            marginRight: 1,
            color: colors.shadowColor
          }}
        />
        <Text
          style={{
            fontSize: 15,
            color: colors.shadowColor
          }}
        >
          {locationText}
        </Text>
      </View>
    );
  }
}

export default UserLocation;
