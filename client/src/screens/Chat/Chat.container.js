import React from 'react';
import { KeyboardAvoidingView } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Query } from 'react-apollo';
import ME from '../../graphql/queries/me';
import GET_CHAT from '../../graphql/queries/getChat';
import Spinner from '../../components/Spinner';
import Error from '../../components/Error';
import ChatContent from './Chat.content';
import ChatCompose from './Chat.compose';
import { keyboardOffsetPosition } from '../../lib/styles';

class Chat extends React.PureComponent {
  render() {
    const { navigation } = this.props;
    const { chatId } = navigation.state.params;
    return (
      <Query query={ME}>
        {({ data: { me } }) => {
          if (!me) return null;
          return (
            <Query query={GET_CHAT} variables={{ _id: chatId }}>
              {({ loading, error, data }) => {
                if (loading) return <Spinner />;
                if (error) return <Error />;
                return (
                  <KeyboardAvoidingView
                    enabled
                    behavior="position"
                    keyboardVerticalOffset={keyboardOffsetPosition}
                    contentContainerStyle={{ backgroundColor: '#ededed' }}
                  >
                    <ChatContent chat={data.getChat} me={me} />
                    <ChatCompose chat={data.getChat} me={me} />
                  </KeyboardAvoidingView>
                );
              }}
            </Query>
          );
        }}
      </Query>
    );
  }
}

export default withNavigation(Chat);
