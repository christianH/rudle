import React from 'react';
import {
  View,
  FlatList,
  Text,
  StyleSheet,
  TouchableOpacity,
  AppState
} from 'react-native';
import dayjs from 'dayjs';
import Hyperlink from 'react-native-hyperlink';
import { ListItem } from 'react-native-elements';
import { withNavigation } from 'react-navigation';
import { Query } from 'react-apollo';
import GET_ACTIVITY from '../../graphql/queries/getActivity';
import GET_CHAT from '../../graphql/queries/getChat';
import MARK_CHAT_AS_SEEN from '../../graphql/mutations/markChatAsSeen';
import Client from '../../apollo';
import {
  colors,
  gutter,
  composeHeight,
  bottomSpace,
  bold,
  windowHeight
} from '../../lib/styles';

const styles = StyleSheet.create({
  contentContainer: {
    minHeight: windowHeight
  },
  msgContainer: {
    backgroundColor: 'transparent',
    flex: 1,
    paddingTop: 0,
    paddingBottom: 0,
    paddingHorizontal: gutter
  },
  msgContainerIncoming: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  msgContainerOutgoing: {
    alignItems: 'flex-end',
    justifyContent: 'flex-end'
  },
  msg: {
    flex: 0,
    paddingHorizontal: gutter,
    paddingVertical: gutter / 2,
    marginBottom: gutter / 2,
    borderTopLeftRadius: gutter,
    borderTopRightRadius: gutter
  },
  msgIncoming: {
    borderBottomLeftRadius: gutter,
    borderBottomRightRadius: gutter,
    borderTopLeftRadius: 0,
    backgroundColor: colors.backgroundColor,
    marginLeft: 0,
    marginRight: gutter * 4,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: colors.shadowColor
  },
  msgOutgoing: {
    borderBottomLeftRadius: gutter,
    borderBottomRightRadius: 0,
    backgroundColor: colors.tintColor,
    marginLeft: gutter * 4,
    marginRight: 0
  },
  msgText: {
    fontSize: 15,
    lineHeight: 20
  },
  msgTextIncoming: {
    color: '#000'
  },
  msgTextOutgoing: {
    color: '#fff'
  },
  msgTextTime: {
    fontSize: 10,
    color: colors.shadowColor,
    position: 'absolute',
    bottom: 5,
    right: 10
  },
  msgInfoTextContainer: {
    backgroundColor: 'transparent',
    flex: 1,
    paddingTop: gutter,
    paddingBottom: gutter / 4
  },
  msgInfoText: {
    color: colors.tintColor,
    fontSize: 13,
    lineHeight: 20
  },
  card: {
    width: 180,
    height: 250,
    marginBottom: -gutter,
    borderRadius: gutter / 2
  },
  cardBackground: {
    flex: 1,
    borderRadius: gutter / 2
  },
  cardTitleContainer: {
    marginTop: gutter * 5,
    alignItems: 'center'
  },
  cardTitle: {
    fontSize: 15,
    ...bold
  },
  cardSubtitle: {
    fontSize: 10
  }
});

class ChatContent extends React.PureComponent {
  componentDidMount() {
    const { navigation } = this.props;
    AppState.addEventListener('change', this.handleAppStateChange);
    this.componentDidFocus = navigation.addListener('didFocus', () => {
      this.markChatAsSeen();
    });
    this.componentWillBlur = navigation.addListener('willBlur', () => {
      this.markChatAsSeen();
    });
  }

  componentDidUpdate(prevProps) {
    const { chat } = this.props;
    if (chat.messages.length !== prevProps.chat.messages.length) {
      this.markChatAsSeen();
    }
  }

  componentWillUnmount() {
    this.componentDidFocus.remove();
    this.componentWillBlur.remove();
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  handleAppStateChange = nextAppState => {
    if (nextAppState === 'active') {
      this.markChatAsSeen();
    }
  };

  markChatAsSeen = () => {
    const { chat, me } = this.props;
    if (!chat.seenBy.includes(me._id)) {
      Client.mutate({
        mutation: MARK_CHAT_AS_SEEN,
        variables: { chatId: chat._id },
        optimisticResponse: {
          __typename: 'Mutation',
          markChatAsSeen: {
            __typename: 'Status',
            message: 'Chat Marked as Seen!'
          }
        },
        update: cache => {
          const prev = cache.readQuery({
            query: GET_CHAT,
            variables: { _id: chat._id }
          });
          const next = prev;
          next.getChat.seenBy = [me._id];
          cache.writeQuery({ query: GET_CHAT, data: next });
        }
      });
    }
  };

  renderListHeader = () => (
    <View style={{ height: composeHeight + bottomSpace + gutter }} />
  );

  renderListFooter = () => <View style={{ height: gutter }} />;

  renderListItem = ({ item }) => {
    const { me } = this.props;
    return (
      <View>
        {item.activityId ? this.renderInfoText(item) : null}
        <View
          style={[
            styles.msgContainer,
            item.from === me._id
              ? styles.msgContainerOutgoing
              : styles.msgContainerIncoming
          ]}
        >
          <View
            style={[
              styles.msg,
              item.from === me._id ? styles.msgOutgoing : styles.msgIncoming
            ]}
          >
            <Hyperlink
              linkDefault
              linkStyle={{
                textDecorationLine: 'underline'
              }}
            >
              <Text
                selectable
                style={[
                  styles.msgText,
                  item.from === me._id
                    ? styles.msgTextOutgoing
                    : styles.msgTextIncoming
                ]}
              >
                {`${item.text}\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0`}
              </Text>
            </Hyperlink>
            <Text style={styles.msgTextTime}>
              {dayjs(item.createdAt).format('HH:mm')}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  renderInfoText = item => {
    const { navigation, me } = this.props;

    return (
      <Query query={GET_ACTIVITY} variables={{ _id: item.activityId }}>
        {({ data }) => {
          if (!data.getActivity) return null;
          return (
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('Activity', {
                  activityId: data.getActivity._id
                })
              }
            >
              <ListItem
                key={item.id}
                title={
                  <Text style={styles.msgInfoText}>
                    {item.from === me._id
                      ? `Du hast auf "${data.getActivity.title}" geantwortet`
                      : `Antwort auf "${data.getActivity.title}" erhalten`}
                  </Text>
                }
                containerStyle={[
                  styles.msgInfoTextContainer,
                  {
                    justifyContent:
                      item.from === me._id ? 'flex-end' : 'flex-start'
                  }
                ]}
                contentContainerStyle={{
                  flex: 0
                }}
              />
            </TouchableOpacity>
          );
        }}
      </Query>
    );
  };

  render() {
    const { chat } = this.props;
    return (
      <FlatList
        data={chat.messages}
        keyExtractor={item => item._id}
        inverted
        renderItem={this.renderListItem}
        initialNumToRender={30}
        ListHeaderComponent={this.renderListHeader}
        ListFooterComponent={this.renderListFooter}
        contentContainerStyle={styles.contentContainer}
        // onEndReached={() => loadMore()}
        // onEndReachedThreshold={0.5}
      />
    );
  }
}

export default withNavigation(ChatContent);
