import React from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Platform
} from 'react-native';
import { withNavigation } from 'react-navigation';
import Icon from '@expo/vector-icons/MaterialIcons';
import uuidV4 from 'uuid/v4';
import { Mutation } from 'react-apollo';
import SEND_MESSAGE from '../../graphql/mutations/sendMessage';
import GET_CHATS from '../../graphql/queries/getChats';
import {
  colors,
  windowWidth,
  gutter,
  bottomSpace,
  composeHeight
} from '../../lib/styles';

const styles = StyleSheet.create({
  composeContainer: {
    position: 'absolute',
    bottom: 0,
    width: windowWidth,
    paddingBottom: bottomSpace,
    backgroundColor: colors.backgroundColor,
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: colors.secondaryColor
  },
  composeWrapper: {
    flexDirection: 'row',
    alignItems: 'flex-end'
  },
  textInput: {
    flex: 1,
    fontSize: 15,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: colors.shadowColor,
    borderRadius: 20,
    marginVertical: gutter / 2,
    paddingHorizontal: 10,
    paddingTop: Platform.OS === 'android' ? 5 : 8,
    paddingBottom: Platform.OS === 'android' ? 5 : 8
  },
  iconLeftContainer: {
    height: composeHeight,
    paddingHorizontal: gutter - 4, // fix for smaller icon
    justifyContent: 'center',
    alignItems: 'center'
  },
  iconRightContainer: {
    height: composeHeight,
    paddingHorizontal: gutter,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

class ChatCompose extends React.PureComponent {
  state = {
    msgText: null
  };

  render() {
    const { msgText } = this.state;
    const { placeholder, chat, me } = this.props;
    return (
      <View style={styles.composeContainer}>
        <View style={styles.composeWrapper}>
          <TouchableOpacity
            style={styles.iconLeftContainer}
            // TODO: navigate to popup add (loaction: siehe Whatsapp?)
            onPress={() => console.log('Chat Compose Add')}
          >
            <Icon name="add" size={28} color={colors.tintColor} />
          </TouchableOpacity>
          <TextInput
            style={styles.textInput}
            onChangeText={newMsgText => this.setState({ msgText: newMsgText })}
            value={msgText}
            placeholder={placeholder}
            placeholderTextColor={colors.shadowColor}
            selectionColor={colors.tintColor}
            tintColor={colors.tintColor}
            underlineColorAndroid="transparent"
            multiline
            maxHeight={99}
          />
          <Mutation mutation={SEND_MESSAGE}>
            {sendMessage => (
              <TouchableOpacity
                style={styles.iconRightContainer}
                onPress={() => {
                  if (msgText) {
                    const timeStamp = new Date();
                    const userId = chat.members.find(
                      member => member !== me._id
                    );
                    sendMessage({
                      variables: {
                        userId,
                        text: msgText.trim(),
                        createdAt: timeStamp
                      },
                      optimisticResponse: {
                        __typename: 'Mutation',
                        sendMessage: {
                          __typename: 'Message',
                          _id: uuidV4(),
                          chatId: uuidV4()
                        }
                      },
                      // eslint-disable-next-line no-shadow
                      update: (cache, { data: { sendMessage } }) => {
                        const prev = cache.readQuery({ query: GET_CHATS });
                        const next = prev;
                        const index = prev.getChats.findIndex(
                          chatObj => chatObj._id === chat._id
                        );
                        const chatObj = prev.getChats[index];
                        chatObj.messages.unshift({
                          __typename: 'Message',
                          _id: sendMessage._id,
                          from: me._id,
                          activityId: null,
                          text: msgText.trim(),
                          createdAt: timeStamp
                        });
                        chatObj.lastMessageAt = timeStamp;
                        chatObj.seenBy = [me._id];

                        if (index >= 0) next.getChats.splice(index, 1);
                        next.getChats.unshift(chatObj);
                        cache.writeQuery({ query: GET_CHATS, data: next });
                      }
                    });
                    this.setState({ msgText: null });
                  }
                }}
              >
                <Icon
                  name="send"
                  size={24}
                  color={msgText ? colors.tintColor : colors.tintColor}
                />
              </TouchableOpacity>
            )}
          </Mutation>
        </View>
      </View>
    );
  }
}

export default withNavigation(ChatCompose);
